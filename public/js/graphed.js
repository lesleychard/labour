function demographyChart() {
	$.get('/demography/getChart', function(data){
		$('#demography-chart').highcharts({
	        chart: {
	            zoomType: 'xy'
	        },
	        title: {
	            text: ''
	        },
	        xAxis: [{
	            categories: data.cats,
	            crosshair: true,
	            tickInterval: 2
	        }],
	        yAxis: [{ // Primary yAxis
	            title: {
	                text: '# of People'
	            }
	        }],
	        tooltip: {
	            shared: true
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            x: 0,
	            verticalAlign: 'top',
	            y: 0,
	            floating: true,
	        },
	        series: [{
	            name: 'Actual Population',
	            type: 'column',
	            data: data.series,
	            color: '#26b0cc'
	        }, {
	            name: 'Low Growth',
	            type: 'line',
	            data: data.idealAge,
	            color: '#eed16d',
	            lineWidth: 3,
	            marker: {
	            	enabled: false
	            }
	        }]
	    });
	});
}

function employmentChart(id) {
	$.get('/employment/getChart/' + id, function(data){
		var tempScrollTop = $(window).scrollTop();
		$('#industry-chart').highcharts({
			chart: {
				type: 'scatter',
				style: {
					fontFamily: 'Ubuntu',
					fontWeight: 300
				}
			},
			plotOptions: {
				scatter: {
					lineWidth: 4,
					color: '#23c08b',
					marker : {
						fillColor: '#39e6ac',
						radius: 5
					}
				}
			},
			title: {
				text: ''
			},
			series: [{
				data: data,
				name: 'Employment in Industry by Year'
			}],
			legend: {
				//enabled: false
			},
			yAxis: {
				title: {
					text: '# of Employees'
				}
			},
			xAxis: {
				title: {
					text: 'Date'
				},
				//tickInterval: 2,
				type: 'datetime'
			},
			tooltip: {
				formatter : function() {
					 var monthNames = [
				        "January", "February", "March",
				        "April", "May", "June", "July",
				        "August", "September", "October",
				        "November", "December"
				    ];
					var d =  new Date(this.x);
					return 	'<strong>Date: </strong>' + monthNames[d.getMonth()] + ' ' + d.getFullYear() + '<br />' +
							'<strong>Employees: </strong>' + this.y + '<br />'
				}
			}
		});
		$(window).scrollTop(tempScrollTop);
	});
}

$(document).on('change', '#industry-select select', function(e){
	e.preventDefault();
	var id = $(this).val();
	employmentChart(id);
	getTopOccupations(id);
	getOccupationChart(id);
});

var graphedMap, graphedTopoLayer, region;

function graphedMapInitialize() {

	graphedMap = L.map('graphed-map', {
		zoomControl: false,
		scrollWheelZoom: false
	}).fitBounds([
		[60.149787, -74.100890],
		[43.703738, -51.425110]
	]);

	var basemap = L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.{ext}', {
		attribution: '',
		subdomains: 'abcd',
		minZoom: 0,
		maxZoom: 20,
		ext: 'png',
		opacity: 0.3
	}).addTo(graphedMap);

	L.control.zoom({
		position:'bottomleft'
	}).addTo(graphedMap);

	spinner.spin();
	$.get('/region/getSelected', function(data){
		region = data.id;
		boundMap(graphedMap, data.prov);

		//Adds TopoJSON data to map
		$.getJSON('js/bnd.json', function(data){
			spinner.stop();
			addTopoData(data);
		})

		function addTopoData(topoData){ 
	  		graphedTopoLayer = new L.TopoJSON(topoData, {
			    onEachFeature: handleGraphedFeature
			}).addTo(graphedMap);
		}
	});
}

function handleGraphedFeature(feature, layer) {
	layer.on({
		mouseover: enterGraphedFeature,
		mouseout: leaveGraphedFeature,
		click: clickGraphedFeature
	});

	layer.setStyle({
		fillColor: '#fff',
		fillOpacity: 0.3,
		color: '#485558',
		weight: 1
	});

	$.get('/region/getSelected', function(data){
		if(feature.id == data.id) {
			layer.setStyle({
	            fillColor: '#26b0cc'
	        });
		}
	});
}

function enterGraphedFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 2,
		fillOpacity: 0.5
	});
}

function leaveGraphedFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 1,
		fillOpacity: 0.3
	});
}

function clickGraphedFeature(e) {
	graphedTopoLayer.setStyle({
		fillColor: '#fff',
		fillOpacity: 0.3
	});

	var layer = e.target;
	$.post('/region/set', {id: layer.feature.id}, function(data) {
		//set fill colour to blue
		layer.setStyle({
			fillColor: '#26b0cc'
		});

		boundMap(graphedMap, data.prov);

		getContent('graphed');

		$.get('/region/getByProv/' + data.prov + '/' + layer.feature.id, function(data){
			$('.location-select-inputs').html(data);
		});
	});
}

$(document).on('change', '.location-select .side-fer select', function(e) {
	var val = $(this).val();
	$.post('/region/set', {id: val}, function(data) {
		$.get('/region/getByProv/' + data.prov + '/' + val, function(data){
			$('.location-select-inputs').html(data);
			graphedTopoLayer.setStyle({
				fillColor: '#fff',
				fillOpacity: 0.3
			});
			graphedTopoLayer.eachLayer(function(layer) {  
				if(layer.feature.id == val) {    
					layer.setStyle({
						fillColor: '#26b0cc'
					});
				}
			});
			getContent('graphed');
		});
	});
});

$(document).on('change', '.location-select .side-prov select', function(e){
	var val = $(this).val();
	$.post('/province/set', {prov: val}, function(data){
		$.get('/region/getByProv/' + val, function(data){
			$('.location-select-inputs').html(data);
			graphedTopoLayer.setStyle({
				fillColor: '#fff',
				fillOpacity: 0.3
			});
			getContent('graphed');
			boundMap(graphedMap, val);
		});
	});
});

$(document).on('click', '.top-industry-item', function(e){
	e.preventDefault();
	var id = $(this).data('id');
	$('#industry-select select').val(id);
	employmentChart(id);
	getTopOccupations(id);
	getOccupationChart(id)
});

function getTopOccupations(industry) {
	$.get('/employIndOcc/getByInd/' + industry, function(data){
		$.get('/employInd/getById/' + industry, function(indRes){
			$('#top-ind-occupations').html(data);
			$('#current-industry').html(indRes.name);
		});
	});
}

function getOccupationChart(industry) {
	$.get('/employInd/getOccupationChart/' + industry, function(data){
		$('#occupations-chart').highcharts({
            chart: {
                type: 'pie',
                style: {
                    fontFamily: 'Ubuntu',
                    fontWeight: 700
                },
                height: 400
            },
            colors: ['#485558', '#26b0cc', '#eed16d', '#23c08b'],
            plotOptions: {
                pie: {
                    showInLegend: true,
                    cursor: 'pointer'
                }
            },
            title: {
                text: 'Occupation Breakdown of Industry',
                enabled: true,
                style: {
                	color: '#485558'
                }
            },
            series: [{
                data: data,
                dataLabels: {
                    formatter: function(){
                        return this.percentage.toPrecision(3) + '%';
                    },
                    distance: 10,
                    color: '#485558',
                    connectorWidth: 2
                },
                name: 'Ratio of Industry'
            }],
            legend: {
                enabled: false
            },
            tooltip: {
            	formatter: function(){
            		return 	this.point.name + '<br />' +
            				this.percentage.toPrecision(3) + '%';
            	}
            }
        });
	});
}

$(document).on('click', '#top-ind-occupations > li > a, .occ-list > li > a', function(e){
	e.preventDefault;
	var id = $(this).data('id');
	$.post('/occupation/set', {id: id}, function(data){
		getSidebar('tabular', getContent('tabular'));
		changeNav('tabular');
	});
});