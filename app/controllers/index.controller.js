var navNames = ['mapped', 'graphed', 'tabular', 'linked'];
var provinces = require('../models/provinces.model.js'); 
var occupations = require('../models/occupations.model.js'); 
var demographies = require('../models/demographies.model.js');
var employment = require('../models/employment.model.js');
var regions = require('../models/regions.model.js');
var links = require('../models/links.model.js');
var regionOccupation = require('../models/region_occupations.model.js');

var index = {

	main : function(req, res) {

		/*req.session = {};
		req.session.intros = {
            'mapped': true,
            'graphed': true,
            'tabular': true,
            'linked': true
        };*/

		//generate nav items list if session var doesn't exist
		var navItems = req.session.navItems;
		if(navItems == undefined) {
			navItems = [];
			for(var i = 0; i < navNames.length; i++) {
				navItems[i] = { 
					name: navNames[i],
					active: false
				};
			}
			navItems[0].active = true;
		}

		req.session.navItems = navItems;

		//generate default session variables for new sessions
		var sessionDefaults = [
			{ name: 'region', default: null },
			{ name: 'prov', default: 0 },
			{ name: 'industry', default: false },
			{ name: 'occupationIds', default: [] },
			{ name: 'occupationTags', default: [] },
			{ name: 'filterOptions', default: [] },
			{ name: 'partner', default: null },
			{ name: 'partnerProv', default: 0 },
			{ name: 'direction', default: "from" },
			//{ name:, default: },
		];
		for(var i = 0; i < sessionDefaults.length; i++) {
			if(req.session[sessionDefaults[i].name] == undefined) {
				req.session[sessionDefaults[i].name] = sessionDefaults[i].default;
			}
		}

	    res.render('index/main', {
	        title: 'Atlantic Labour Force',
	        navItems: navItems
	    });
	},

	setNav : function(req, res) {
		var newNav = req.body.name,
			navItems = req.session.navItems;

		for(var i = 0; i < navNames.length; i++) {
			navItems[i].active = false;
		}
		navItems[navNames.indexOf(newNav)].active = true;

		req.session.navItems = navItems;

		res.render('index/nav', {
			navItems: navItems
		});
	},

	mapped : function(req, res) {
		res.render('index/mapped', {
			intro: req.session.intros['mapped']
		});
		req.session.intros['mapped'] = false;
	},

	mappedSide : function(req, res) {
		var region = req.session.region,
			prov = req.session.prov;

		demographies.getPopulation(region, prov, function(pops){
			var weightedSum = 0;
			for(var i = 0; i < 85; i = i + 5) {
				var cohort = 'pop_' + i + '_' + (i + 4);
				weightedSum += (i + 2.5) * pops[0][cohort];
			}
			weightedSum += 87.5 * pops[0]['pop_85'];
			meanAge = Math.round(weightedSum / pops[0]['pop_total']);
			console.log(meanAge);

			regionOccupation.findTop(region, prov, 3, function(topOccs){
				if(region == null) {
					provinces.findById(prov, function(provRes) {
						res.render('index/mappedSide', {
							region: provRes[0],
							topOccs: topOccs,
							pops: pops[0],
							isRegion: false,
							meanAge: meanAge
						});
					});		
				} else {
					regions.findById(region, function(regRes){
						res.render('index/mappedSide', {
							region: regRes[0],
							topOccs: topOccs,
							pops: pops[0],
							isRegion: true,
							meanAge: meanAge
						});
					});
				}
			});
		});
	},

	tabular : function(req, res) {
		var occupationIds = req.session.occupationIds,
			occupationTags = req.session.occupationTags,
			prov = req.session.prov,
			filterOptions = req.session.filterOptions;

		if(!prov) prov = req.session.prov = 0;

		occupations.filter(occupationIds, occupationTags, filterOptions, prov, function(occupationsList){
			occupations.findByIds(occupationIds, function(selectedOccupations){
				res.render('index/tabular', {
					resultNum : occupationsList.length,
					occupations : occupationsList,
					selectedOccupations : selectedOccupations,
					occupationTags : req.session.occupationTags,
					hasOccFilter: (occupationIds.length > 0),
					hasOccTags: (occupationTags.length > 0),
					isAll : (prov == 0),
					intro: req.session.intros['tabular']
				});
				req.session.intros['tabular'] = false;
			});
		});
	},

	tabularSide : function(req, res) {
		var provId = req.session.prov;
		if(!provId) provId = req.session.prov = 0;

		provinces.findAll(function(provs){
			for(var i = 0; i < provs.length; i++) {
				provs[i].active = (provs[i].province_id == provId);
			}
			res.render('index/tabularSide', {
				provs: provs
			});
		})
	},

	graphed : function(req, res) {
		var prov = req.session.prov,
			region = req.session.region;

		demographies.getPopulation(region, prov, function(demRes){
			if(prov == 0) demRes[0].region_name = 'All Atlantic';
			employment.findIndustries(region, prov, function(emRes){
				var pop = demRes[0]['pop_total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				var industries = [];
				var curInd = req.session.industry;

				if(!curInd) curInd = req.session.industry = emRes[0].employ_industry_id;

				for(var i = 0; i < emRes.length; i++) {
					industries.push({
						id: emRes[i].employ_industry_id,
						name: emRes[i].name,
						active: (emRes[i].employ_industry_id == curInd)
					});
				}
				employment.findTopIndustries(region, prov, 8, function(topIndRes){
					employment.findMaxYear(region, prov, function(maxYear){
						demographies.getRatios(region, prov, function(ratios){
							provinces.findById(prov, function(provRes){
								if(region == null) {
									res.render('index/graphed', {
										prov: provRes[0].name,
										region: region,
										pop: pop,
										industries: industries,
										topInd: topIndRes,
										ratios: ratios[0],
										max_year: maxYear[0]['max_year'],
										intro: req.session.intros['graphed']
									});
									req.session.intros['graphed'] = false;
								} else {
									regions.findById(region, function(regRes){
										res.render('index/graphed', {
											prov: provRes[0].name,
											region: regRes[0].name,
											pop: pop,
											industries: industries,
											topInd: topIndRes,
											ratios: ratios[0],
											max_year: maxYear[0]['max_year'],
											intro: req.session.intros['graphed']
										});
										req.session.intros['graphed'] = false;
									});
								}
							});
						});		
					});							
				});
			});
		});
	},

	graphedSide : function(req, res) {
		var prov = req.session.prov;
		regions.findByProv(prov, function(regRes){
			var regList = [],
				provList = [],
				region = req.session.region;

			if(prov != 0) {
				regList.push({
					id: 'all',
					name: 'All ' + regRes[0]['prov_name'],
					selected: (region == null)
				});
			}

			for(var i = 0; i < regRes.length; i++) {
				regList.push({
					id: regRes[i]['region_id'],
					name: regRes[i]['region_name'],
					selected: (regRes[i]['region_id'] == region)
				});
			}

			provinces.findAll(function(provRes){
				regionOccupation.findTop(region, prov, 5, function(topOccRes){
					for(var i = 0; i < provRes.length; i++) {
						provList.push({
							id : provRes[i]['province_id'],
							name : provRes[i]['abbr'],
							selected : (provRes[i]['province_id'] == prov)
						});
					}

					res.render('index/graphedSide', {
						regions: regList,
						provs: provList,
						topOccs: topOccRes
					});
				});
			});
		});
	},

	linked : function(req, res) {
		var region = req.session.region;
		res.render('index/linked', {
			isProv: (region == null),
			intro: req.session.intros['linked']
		});
		req.session.intros['linked'] = false;
	},

	linkedSide : function(req, res) {
		var direction = req.session.direction,
			region = req.session.region,
			partner = req.session.partner,
			partnerProv = req.session.partnerProv,
			prov = req.session.prov,
			linkInd = req.session.linkIndustry,
			linkCom = req.session.linkCommodity;

		regions.findWithProv(region, prov, function(regRes){
			var directions = [
				{
					name: 'From ' + regRes[0].name,
					value: 'from',
					active: (direction == "from")
				},
				{
					name: 'To ' + regRes[0].name,
					value: 'to',
					active: (direction == "to")
				}
			];

			regions.findByProv(partnerProv, function(regResTwo){
				var regList = [],
					provList = [];

				if(partnerProv != 0) {
					regList.push({
						id: 'all',
						name: 'All ' + regResTwo[0]['prov_name'],
						selected: (partner == null)
					});
				}

				for(var i = 0; i < regResTwo.length; i++) {
					regList.push({
						id: regResTwo[i]['region_id'],
						name: regResTwo[i]['region_name'],
						selected: (regResTwo[i]['region_id'] == partner)
					});
				}

				provinces.findAll(function(provRes){
					for(var i = 0; i < provRes.length; i++) {
						provList.push({
							id : provRes[i]['province_id'],
							name : provRes[i]['abbr'],
							selected : (provRes[i]['province_id'] == partnerProv)
						});
					}

					links.findIndustries(region, prov, partner, partnerProv, direction, function(indRes){
						if(linkInd == undefined) linkInd = req.session.linkIndustry = null;
						var indList = [];
						indList.push({
							id: 'all',
							name: 'All Industries',
							active: (linkInd == null)
						});
						var foundInd = false;
						for(var i = 0; i < indRes.length; i++) {
							indList.push({
								id: indRes[i].id,
								name: indRes[i].name,
								active: (indRes[i].id == linkInd)
							});
							if(indRes[i].id == linkInd) foundInd = true;
						}
						if(!foundInd) linkInd = req.session.linkIndustry = null;

						links.findCommodities(region, prov, partner, partnerProv, direction, function(comRes){
							if(linkCom == undefined) linkCom = req.session.linkCommodity = null;
							var comList = [];
							comList.push({
								id: 'all',
								name: 'All Commodities',
								active: (linkCom == null)
							});
							var foundCom = false;
							for(var i = 0; i < comRes.length; i++) {
								comList.push({
									id: comRes[i].id,
									name: comRes[i].name,
									active: (comRes[i].id == linkCom)
								});
								if(comRes[i].id == linkCom) foundCom = true;
							}
							if(!foundCom) linkCom = req.session.linkCommodity = null;
							res.render('index/linkedSide', {
								directions: directions,
								disableDir: (partner != null),
								isAll: (partnerProv == 0),
								isProv: (region == null),
								regions: regList,
								provs: provList,
								industries: indList,
								commodities: comList
							});
						});
					});
				});
			});
		});
	}
}

module.exports = index;

