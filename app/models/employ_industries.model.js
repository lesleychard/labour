var appRoot = require('app-root-path');
var conn = require(appRoot + '/config/sql.js');

var employIndustries = {
	findById : function(id, callback) {
		var query = 'SELECT * FROM employ_industries WHERE ' +
					'employ_industry_id = ' + id;
		conn.execute(query, callback);
	}
}

module.exports = employIndustries;