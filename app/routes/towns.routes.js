module.exports = function(app) {
    var town = require('../controllers/towns.controller');
    app.get('/town/getByRegion/:region', town.getByRegion);
};