var employIndustryOccupations = require('../models/employ_industry_occupations.model.js');
var employIndustries = require('../models/employ_industries.model.js');

var employIndustryOccupation = {
	getByInd : function(req, res) {
		var industry = req.params.id;

		employIndustries.findById(industry, function(indRes){
			if(indRes[0] != undefined) {
				var indCode = indRes[0].three_code;
				employIndustryOccupations.findByInd(indCode, 8, function(occs){
					res.render('employ_industry_occupations/getByInd', {
						occs: occs
					})
				});
			}
		});
	}
}

module.exports = employIndustryOccupation;