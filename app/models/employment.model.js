var appRoot = require('app-root-path');
var conn = require(appRoot + '/config/sql.js');

var employment = {

	findIndustries : function(region, prov, callback) {
		var query;
		
		if(region == null) {
			query = 'SELECT employ_industries.employ_industry_id, employ_industries.name FROM prov_employment ' +
					'JOIN employ_industries ON prov_employment.employ_industry_id = employ_industries.employ_industry_id ';
			if(prov != 0) query += 'WHERE prov_employment.province_id = ' + prov + ' ';
			query += 'GROUP BY employ_industries.name';
		} else {
			query = 'SELECT employ_industries.employ_industry_id, employ_industries.name FROM employment ' +
					'JOIN employ_industries ON employment.employ_industry_id = employ_industries.employ_industry_id ' +
					'WHERE employment.region_id = ' + region + ' GROUP BY employ_industries.name';
		}
		conn.execute(query, callback);
	},

	findSeries : function(region, prov, industry, callback) {
		var query;

		if(region == null) {
			if(prov == 0) {
				query = 'SELECT SUM(number_employees) AS number_employees, year ' + 
						'FROM prov_employment ' +
						'WHERE employ_industry_id = ' + industry + ' ' +
						'GROUP BY year ORDER BY year';
			} else {
				query = 'SELECT * FROM prov_employment WHERE ' +
					'province_id = ' + prov + ' AND employ_industry_id = ' + industry +
					' GROUP BY year ORDER BY year';
			}
		} else {
			query = 'SELECT * FROM employment WHERE ' +
					'region_id = ' + region + ' AND employ_industry_id = ' + industry +
					' GROUP BY year ORDER BY year';
		}
		conn.execute(query, callback);
	},

	findTopIndustries : function(region, prov, limit, callback) {
		var query;
		if(region == null) {
			query = 'SELECT employ_industries.employ_industry_id AS id, ' +
					'employ_industries.name AS name, ' +
					'prov_employment.number_employees AS number_employees ' + 
					'FROM prov_employment ' +
					'JOIN employ_industries ON prov_employment.employ_industry_id = employ_industries.employ_industry_id ';
			if(prov != 0) query += 'WHERE prov_employment.province_id = ' + prov + ' ';
			query += ' AND YEAR(prov_employment.year) = ' + 
					 '(SELECT MAX(YEAR(prov_employment.year)) FROM prov_employment';
			if(prov != 0) query += ' WHERE prov_employment.province_id = ' + prov;
			query += ') GROUP BY prov_employment.employ_industry_id ORDER BY number_employees DESC LIMIT ' + limit;
		} else {
			query = 'SELECT employ_industries.employ_industry_id AS id, ' +
					'employ_industries.name AS name, ' +
					'employment.number_employees AS number_employees ' + 
					'FROM employment ' +
					'JOIN employ_industries ON employment.employ_industry_id = employ_industries.employ_industry_id ' +
					'WHERE employment.region_id = ' + region +
			 		' AND YEAR(employment.year) = ' + 
					'(SELECT MAX(YEAR(employment.year)) FROM employment WHERE employment.region_id = ' + region + ') ' +
					'GROUP BY employment.employ_industry_id ORDER BY number_employees DESC LIMIT ' + limit;
		}
		conn.execute(query, callback);
	},

	findMaxYear : function(region, prov, callback) {
		var query;
		if(region == null) {
			query = 'SELECT MAX(YEAR(prov_employment.year)) AS max_year ' +
					'FROM prov_employment';
			if(prov != 0) query += ' WHERE province_id = ' + prov;
		} else {
			query = 'SELECT MAX(YEAR(employment.year)) AS max_year ' +
					'FROM employment WHERE region_id = ' + region;
		}
		conn.execute(query, callback);
	}
}

module.exports = employment;