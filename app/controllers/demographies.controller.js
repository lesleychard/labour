var demographies = require('../models/demographies.model.js');

var demography = {
	getChart : function(req, res) {
		var prov = req.session.prov,
			region = req.session.region,
			findReg = false,
			findProv = false,
			ageCohorts = {
				pop_0_4: 0.062250302,
				pop_5_9: 0.059462975,
				pop_10_14: 0.058533866,
				pop_15_19: 0.0771160457,
				pop_20_24: 0.0706122828,
				pop_25_29: 0.0771160457,
				pop_30_34: 0.0678249559,
				pop_35_39: 0.0631794109,
				pop_40_44: 0.062250302,
				pop_45_49: 0.058533866,
				pop_50_54: 0.0566756481,
				pop_55_59: 0.0641085199,
				pop_60_64: 0.0548174301,
				pop_65_69: 0.0511009941,
				pop_70_74: 0.0445972313,
				pop_75_79: 0.040230419,
				pop_80_84: 0.0232277246,
				pop_85: 0.0083619809
			};

		demographies.get(region, prov, function(demRes){
			var dem = {
				series : [],
				cats : [],
				idealAge : []
			}

			for(var i = 0; i <= 80; i = i + 5) {
				var range = 'pop_' + i + '_' + (i+4);

				dem.series.push({
					y: demRes[0][range],
					name: i + ' to ' + (i+4)
				});
				dem.cats.push(i + ' to ' + (i+4));

				dem.idealAge.push(Math.round(parseFloat(demRes[0]['pop_total']) * ageCohorts[range]));
			}
			dem.series.push({
				y: demRes[0]['pop_85'],
				name: '85 and over'
			});
			dem.cats.push('85 and over');
			dem.idealAge.push(Math.round(parseFloat(demRes[0]['pop_total']) * ageCohorts['pop_85']));
			res.send(dem);
		});
	}
}

module.exports = demography;