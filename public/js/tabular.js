function scrollInits() {
    $('.filter-bar div.filter-industries').slimScroll({
        height: '200px'
    });
}

function skillChart() {
    $.get('/occupation/getSkills', function(data){
        var skills = [
            data[0].SkillCount0,
            data[1].SkillCountA,
            data[2].SkillCountB,
            data[3].SkillCountC,
            data[4].SkillCountD
        ];
        $('#skills-chart').highcharts({
            chart: {
                type: 'pie',
                style: {
                    fontFamily: 'Ubuntu',
                    fontWeight: 700
                }
            },
            plotOptions: {
                pie: {
                    showInLegend: true,
                    cursor: 'pointer'
                }
            },
            title: {
                text: 'Skills Breakdown',
                enabled: false
            },
            series: [{
                data: [{
                    y: skills[0],
                    name: '0',
                    color: 'rgba(0,0,0,0.1)'
                }, {
                    y: skills[1],
                    name: 'A',
                    color: '#26b0cc'
                }, {
                    y: skills[2],
                    name: 'B',
                    color: '#23c08b'
                }, {
                    y: skills[3],
                    name: 'C',
                    color: '#71c9d5'
                }, {
                    y: skills[4],
                    name: 'D',
                    color: '#eed16d'
                }],
                dataLabels: {
                    formatter: function(){
                        return this.percentage.toPrecision(3) + '%';
                    },
                    distance: 10,
                    color: '#fff',
                    connectorWidth: 2
                },
                name: 'Occupations'
            }],
            legend: {
                enabled: true,
                itemStyle: {
                    color: '#fff',
                    borderWidth: 1,
                    borderColor: '#fff'
                }
            }
        });
    });
}

$(document).on('keyup', '#filter-occupations', function() {
    var val = $(this).val();
    showFilterOptions(val);
});

function showFilterOptions(val) {
    if(val == "") {
        $('#filter-occupations-list').removeClass('active');
    } else {
        $('#filter-occupations-list').addClass('active');
        $.get('/occupation/typeAhead/' + val, function(data) {
            if(data.length > 0) {
                $('#filter-occupations-list .no-results').hide();
                var html = '';
                for(var i = 0; i < data.length; i++) {
                    var name = data[i].name,
                        n = name.toUpperCase().indexOf(val.toUpperCase()),
                        m = n + val.length;
                    var format = name.insert(m, '</strong>');
                        format = format.insert(n, '<strong>');
                    html += '<li><a href="#" data-id="' + data[i].occupation_id + '">' + format + '</a></li>';    
                }
                $('#filter-occupations-list .results').html(html);
            } else {
                $('#filter-occupations-list .results').html("");
                $('#filter-occupations-list .no-results').show();
            }
        });
    }
}

$(document).on('focusout', '#filter-occupations', function(){
    $('#filter-occupations-list').removeClass('active');
});

$(document).on('focusin', '#filter-occupations', function(){
    if($(this).val() != "") {
        $('#filter-occupations-list').addClass('active');
    }
});

String.prototype.insert = function (index, string) {
  if (index > 0)
    return this.substring(0, index) + string + this.substring(index, this.length);
  else
    return string + this;
};

$(document).on('click', '#filter-occupations-list a', function(e){
    e.preventDefault();
    var id = $(this).data('id'),
        name = $(this).html();

    $.post('/occupation/set', {id: id}, function(data){
        console.log(data);
        var html = '';
        for(var i = 0; i < data.length; i++) {
            html += '<a href="#" data-id="' + data[i].occupation_id + '">';
            html += data[i].name;
            html += '</a>';
        }
        $('.filter-tags-occupation').html(html).addClass('show');
        getOccupations();
    });
});

$(document).on('click', '.filter-tags-occupation a', function(e){
    e.preventDefault();
    var id = $(this).data('id'),
        el = $(this).fadeOut('slow').remove();
    $.post('/occupation/unset', {id: id}, function(rem){
        if(rem.toString() == "") {
            $('.filter-tags-occupation').removeClass('show');
        }
        getOccupations();
    });
});

$(document).on('submit', '#filter-tags-form', function(e){
    e.preventDefault();
    var val = $('#filter-occupations').val();
    $.post('/occupation/setTag', {tag: val}, function(data){
        var html = '';
        for(var i = 0; i < data.length; i++) {
            html += '<a href="#">';
            html += data[i];
            html += '</a>';
        }
        $('.filter-tags-general').html(html).addClass('show');
        getOccupations();
    })
})

$(document).on('click', '.filter-tags-general a', function(e){
    e.preventDefault();
    var tag = $(this).html(),
        el = $(this).fadeOut('slow').remove();
    $.post('/occupation/unsetTag', {tag: tag}, function(rem){
        if(rem.toString() == "") {
            $('.filter-tags-general').removeClass('show');
        }
        getOccupations();
    });
});

$(document).on('click', '#filter-options', function(e){
    e.preventDefault();
    filterOptions();
    $(this).toggleClass('active');
});

$(document).on('click', '#filter-options-list ul a', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    var optionType = $(this).parent().parent().data('type'),
        optionValue = $(this).data('value');

    $.post('/occupation/setFilterOptions', {optionType: optionType, optionValue: optionValue}, function(data){
        filterOptions();
        getOccupations();
    });
})

function filterOptions() {
    $.get('/occupation/getFilterOptions', function(data) {
        $('#filter-options-list').html(data);
    });
}

function getOccupations() {
    $.get('/occupation/getFiltered', function(data) {
        $('.data-container').html(data);
    });
}

var sideMap, sideTopoLayer;

function sideMapInitialize() {
    sideMap = L.map('side-map', {
        zoomControl: false
    }).fitBounds([
        [60.149787, -74.100890],
        [43.703738, -51.425110]
    ]);

    L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.{ext}', {
        attribution: '',
        subdomains: 'abcd',
        minZoom: 0,
        maxZoom: 20,
        ext: 'png',
        opacity: 0.3
    }).addTo(sideMap);

    $.get('/province/getSelected', function(data) {
        boundMap(sideMap, data.prov);
    });
}

$(document).on('change', '#map-prov', function() {
    var val = this.value;
    $.post('/province/set', {prov: val}, function(data){
        boundMap(sideMap, val);
        getOccupations();
    });
});

$(document).on('click', '.occupation-name', function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $.get('/occupation/getReplPot/' + id, function(data) {
        $('#replpot-area').html(data);
        $('#replPotModal').modal({
            show: true
        });
        $.get('/occupation/getReplPotChart/' + id, function(chartData){
            $('#replpot-chart').highcharts({
                chart: {
                    type: 'column',
                    style: {
                        fontFamily: 'Ubuntu',
                        fontWeight: 300
                    },
                    width: 800
                },
                title: {
                    text: 'Replacement Potential Overview'
                },
                series: [{
                    data: chartData.series,
                    name: 'Replacement Potential',
                    showInLegend: false
                }],
                yAxis: {
                    title: {
                        text: 'Replacement Potential'
                    },
                    labels: {
                        formatter: function() {
                            var vals = ['N/A', 'Very Low', 'Low', 'High', 'Very High'];
                            return vals[this.value];
                        }
                    },
                    min: 0,
                    max: 4
                },
                xAxis: {
                    title: {
                        text: 'Province'
                    },
                    categories: ['All Atlantic', 'Newfoundland & Labrador', 'Prince Edward Island', 'Nova Scotia', 'New Brunswick']
                },
                tooltip: {
                    formatter : function() {
                        var vals = ['N/A', 'Very Low', 'Low', 'High', 'Very High'];
                        return  this.x + '<br />' +
                                '<strong>Replacement Potential:</strong> ' + vals[this.y];
                    }
                }
            });
            console.log(chartData.series);
        });
    });
});