var appRoot = require('app-root-path');
var conn = require(appRoot + '/config/sql.js');

var occupation = {

	findAll : function(callback) {
		var query = 'SELECT * FROM occupations';
		conn.execute(query, callback);
	},

	findByIds : function(ids, callback) {
		var query = 'SELECT * FROM occupations';
		if(ids.length > 0) {
			query += ' WHERE ';
			for(var i = 0; i < ids.length; i++) {
				query += 'occupation_id = ' + ids[i];
				if(i != ids.length - 1) query += ' OR ';
			}
		}
		conn.execute(query, callback);
	},

	filter : function(ids, tags, filters, prov, callback) {
		var query = 'SELECT occupations.name AS name, ' +
					'occupations.occupation_id AS id, ' +
					'occupations.skill_level AS skill_level, ' +
					'prov_occupations.wage_attr AS wage_attr, ' +
					'prov_occupations.repl_pot AS repl_pot ' +
					'FROM occupations JOIN prov_occupations ' +
					'ON occupations.occupation_id = prov_occupations.occupation_id';

		var where = false;
		var filterQueries = {
			skill_level : [
				'occupations.skill_level = "0"',
				'occupations.skill_level = "A"',
				'occupations.skill_level = "B"',
				'occupations.skill_level = "C"',
				'occupations.skill_level = "D"',
			],
			wage_attr: [
				'prov_occupations.wage_attr = "Non-Competitive"',
				'prov_occupations.wage_attr = "Marginally Non-Competitive"',
				'prov_occupations.wage_attr = "Marginally Competitive"',
				'prov_occupations.wage_attr = "Competitive"',
			],
			repl_pot: [
				'prov_occupations.repl_pot = "Very Low"',
				'prov_occupations.repl_pot = "Low"',
				'prov_occupations.repl_pot = "High"',
				'prov_occupations.repl_pot = "Very High"'
			]
		}

		if(ids != undefined && ids.toString() != "") {
			query += ' WHERE (';
			where = true;
			for(var i = 0; i < ids.length; i++) {
				if(i != 0) query += ' OR';
				query += ' occupations.occupation_id = ' + ids[i];
			}

		}

		if(tags != undefined && tags.toString() != "") {
			if(where) {
				query += ' OR';
			} else {
				query += ' WHERE (';
			}
			where = true;
			for(var i = 0; i < tags.length; i++) {
				if(i != 0) query += ' OR';
				query += " occupations.name LIKE '%" + tags[i] + "%'";
			}
		}
		if(where) query += ')';

		if(filters != undefined && filters.toString() != "") {
			var filterTypes = ['skill_level', 'wage_attr', 'repl_pot'];
			for(var i = 0; i < filterTypes.length; i++) {
				var andFlag = false;
				var orFlag = false;
				for(var j = 0; j < filters[filterTypes[i]].length; j++) {
					if(filters[filterTypes[i]][j].active) {
						if(i == 0 && !where) {
							query += ' WHERE (';
							where = true;
							andFlag = true;
						} else {
							if(!andFlag) {
								query += ' AND (';
								andFlag = true;
							}
						}

						if(orFlag) query += ' OR ';
						orFlag = true;
						query += filterQueries[filterTypes[i]][j];
					}
				}
				if(andFlag) query += ')';
			}
		}

		if(!where) {
			query += ' WHERE';
		} else {
			query += ' AND';
		}
		query += ' prov_occupations.province_id = ' + prov;

		query += ' GROUP BY occupations.name'
		//console.log(query);
		conn.execute(query, callback);
	},

	findByKey : function(key, callback) {
		var query = "SELECT * FROM occupations WHERE name LIKE '%" + key + "%' LIMIT 5";
		conn.execute(query, callback);
	},

	countSkills : function(callback) {
		var q = conn.createQueue();
		var skills = ["0", "A", "B", "C", "D"];
		for(var i = 0; i < skills.length; i++) {
			var query = 'SELECT COUNT(*) AS "SkillCount' + skills[i] +'" FROM occupations WHERE skill_level = "' + skills[i] + '"';
			q.query(query, function(err, results) {
				if(err) console.log(err.error);
				callback(results);
			});
		}
		q.execute();
	},

	findReplPot : function(id, callback) {
		var query = 'SELECT DISTINCT ' +
					'occupations.name AS name, occupations.skill_level AS skill_level, ' +
					'prov_occupations.repl_pot AS repl_pot, ' + 
					'provinces.name AS province, provinces.province_id AS province_id FROM occupations ' +
					'JOIN prov_occupations ON occupations.occupation_id = prov_occupations.occupation_id ' +
					'JOIN provinces ON prov_occupations.province_id = provinces.province_id ' +
					'WHERE occupations.occupation_id = ' + id + 
					' ORDER BY provinces.province_id';
		conn.execute(query, callback);
	},

	findById : function(id, callback) {
		var query = 'SELECT * FROM occupations WHERE occupation_id = ' + id;
		conn.execute(query, callback);
	}
};

module.exports = occupation;