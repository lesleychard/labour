var appRoot = require('app-root-path');
var conn = require(appRoot + '/config/sql.js');

var demography = {
	getPopulation : function(region, prov, callback) {
		var query = '';

		if(region == null) {
			if(prov == 0) {
				query += 'SELECT SUM(prov_demographies.pop_total) AS pop_total, ';
				for(var i=0; i < 85; i=i+5) {
					query += 'SUM(prov_demographies.pop_' + i + '_' + (i+4) + ') AS pop_' + i + '_' + (i+4) + ', ';
				}
				query += 'SUM(prov_demographies.pop_85) AS pop_85, ';
				query += 'provinces.name AS region_name ' +
					 	 'FROM prov_demographies JOIN provinces ON prov_demographies.province_id = provinces.province_id';
			} else {
				query += 'SELECT prov_demographies.*, provinces.name AS region_name ' +
					 	 'FROM prov_demographies JOIN provinces ON prov_demographies.province_id = provinces.province_id ' +
					 	 'WHERE prov_demographies.province_id = ' + prov;
			}
			
		} else {
			query += 'SELECT demographies.*, regions.name AS region_name ' +
					 'FROM demographies JOIN regions ON demographies.region_id = regions.region_id ' +
					 'WHERE demographies.region_id = ' + region;
		}
		conn.execute(query, callback);

	},

	get : function(region, prov, callback) {
		var query;
		if(region == null) {
			if(prov == 0) {
				query = 'SELECT SUM(prov_demographies.pop_total) AS pop_total, ';
				for(var i=0; i < 85; i=i+5) {
					query += 'SUM(prov_demographies.pop_' + i + '_' + (i+4) + ') AS pop_' + i + '_' + (i+4) + ', ';
				}
				query += 'SUM(prov_demographies.pop_85) AS pop_85 ';
				query += 'FROM prov_demographies';
			} else {
				query = 'SELECT * FROM prov_demographies WHERE ' +
						'province_id = ' + prov;
			}
		} else {
			query = 'SELECT * FROM demographies WHERE ' +
					'region_id = ' + region;
		}
		conn.execute(query, callback);
	},

	getRatios : function(region, prov, callback) {
		var working = '', notWorking = '', provTable = '';
		if(region == null) provTable = 'prov_';
		for(var i = 0; i < 85; i = i+5) {
			if(i > 14 && i < 65) {
				if(i != 15) working += ' + ';
				working += provTable + 'demographies.pop_' + i + '_' + (i+4);
			} else {
				if(i != 0) notWorking += ' + ';
				notWorking += provTable + 'demographies.pop_' + i + '_' + (i+4);
			}
		}
		notWorking += ' + ' + provTable + 'demographies.pop_85';

		if(region == null) {
			if(prov == 0) {
				var query = 'SELECT ' + 
							'ROUND((SUM(' + working + ')/SUM(prov_demographies.pop_total))*100, 2) AS percent_working, ' +
							'ROUND((SUM(' + notWorking + ')/SUM(' + working + ')*100), 2) AS depend_ratio ' + 
							'FROM prov_demographies ';
			} else {
				var query = 'SELECT ' +
						'ROUND((SUM(' + working + ')/SUM(prov_demographies.pop_total))*100, 2) AS percent_working, ' +
						'ROUND((SUM(' + notWorking + ')/SUM(' + working + ')*100), 2) AS depend_ratio ' + 
						'FROM prov_demographies ' + 
						'WHERE prov_demographies.province_id = ' + prov;
			}
		} else {
			var query = 'SELECT ' +
						'ROUND((SUM(' + working + ')/demographies.pop_total)*100, 2) AS percent_working, ' +
						'ROUND((SUM(' + notWorking + ')/SUM(' + working + ')*100), 2) AS depend_ratio ' + 
						'FROM demographies ' + 
						'WHERE demographies.region_id = ' + region;
		}
		conn.execute(query, callback);
	}
}

module.exports = demography;