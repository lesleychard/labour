var linkedProvince, linkedInfo, linkedDetails, linkedMap, regionLayer, townsLayer;
var townsMarkers, regionLayerGroup, linkLayerGroup, townsMarkerGroup, region, partner, linkPopups;
var partnerMap, partnerTopoLayer, partnerFer;
var view = false;

function linkedMapInitialize() {
	townsMarkers = {};
	linkPopups = {};
	regionLayerGroup = L.featureGroup();
	linkLayerGroup = L.geoJson(false, {
		onEachFeature: handleLink
	});
	townsMarkerGroup = L.featureGroup();

	//Basemap, etc
	var basemap = L.tileLayer('http://a{s}.acetate.geoiq.com/tiles/acetate-base/{z}/{x}/{y}.png', {
		maxZoom: 18,
		attribution: 'Atlantic Labour Force',
		id: 'examples.map-20v6611k'
	});

	$.get('/region/getSelected', function(regionRes){
		$.get('/region/getPartner', function(partnerRes){
			region = regionRes.id;
			partner = partnerRes.id;
			//Adds TopoJSON data to map
			$.getJSON('js/bnd.json')
			  .done(addTopoData);

			function addTopoData(topoData){ 
		  		regionLayer = new L.TopoJSON(topoData, {
				    onEachFeature: handleRegion
				});
				regionLayerGroup.addLayer(regionLayer);
			}
		});
	});

	$.getJSON('js/townpts.json')
		  .done(addTownsData);

	function addTownsData(data) {
		townsLayer = new L.geoJson(data, {
		    pointToLayer: function (feature, latlng) {
		        var marker = L.circleMarker(latlng);
		        townsMarkers[feature.properties.id] = marker;
		        return marker;
		    }
		});
	}

	regionLayerGroup.on('layeradd', function(){
		regionLayerGroup.bringToBack();
	});

	spinner.spin();

	linkedMap = L.map('linked-map', {
		zoomControl: false,
		layers: [basemap, townsMarkerGroup, linkLayerGroup, regionLayerGroup]
	});

	regionLayerGroup.on("layeradd", function(e){
		spinner.stop();
	});

	linkedMap.setView([53,-59.0625], 5);

	//Add zoom control to bottom left
	L.control.zoom({
	     position:'bottomleft'
	}).addTo(linkedMap);

	linkedInfo = L.control({
		position: 'topleft'
	});

	linkedInfo.onAdd = function(map) {
	    this._div = L.DomUtil.create('div', 'linked-info'); // create a div with a class "linkedInfo"
	    this.update();
	    return this._div;
	};

	// method that we will use to update the control based on feature properties passed
	linkedInfo.update = function(name) {
	    this._div.innerHTML = (name ? name : 'Hover over any region');
	};

	linkedInfo.addTo(linkedMap);

	linkedMap.on('zoomend', function() {
	   	getLinks();
	});
}

function handleRegion(feature, layer) {
	layer.on({
		mouseover: enterRegion,
		mouseout: leaveRegion,
		click: clickRegion
	});

	layer.setStyle({
		fillColor: '#39e6ac',
		fillOpacity: 0.6,
		color: '#7D7D7D',
		weight: 1
	});

	if(feature.id == region) {
		layer.setStyle({
            fillColor: '#26b0cc'
        });
	}

	if(feature.id == partner) {
		layer.setStyle({
            fillColor: '#eed16d'
        });
	}
}

function enterRegion(e) {
	var layer = e.target;
	layer.setStyle({
		fillOpacity: 1
	});
	$.get('/region/get/' + layer.feature.id, function(data) {
		linkedInfo.update(data['name']);
	})
}

function leaveRegion(e) {
	var layer = e.target
	layer.setStyle({
		fillOpacity: 0.6
	});
	linkedInfo.update();
}

function clickRegion(e) {
	var layer = e.target;

	regionLayerGroup.setStyle({
		fillColor: '#39e6ac'
	});

	layer.setStyle({
        fillColor: '#26b0cc'
    });

	$.post('/region/set', {id: layer.feature.id}, function(data){
		region = layer.feature.id;
		refreshLinks();
		//getContent('linked');
		//flowInit();
		//setDirection(data[0].FIRST_THRE);
	});
}

function setDirection(fer) {
	$('#link-direction #to').html('To ' + fer);
	$('#link-direction #from').html('From ' + fer);
}

function getLinks() {
	//get link direction
	//spinner.spin();
	$.get('/link/getDirection', function(direction){
		//we haven't plotted the selected FER's town yet
		var townPlotted = false;
		var linkCollection = [];

		//get unique links
		$.get('/link/getLinks', function(data){
			//clear previous loaded data on link and towns layers
			linkLayerGroup.clearLayers();
			townsMarkerGroup.clearLayers();

			//for each returned link
			for(var i = 0; i < data.length; i++) {

				//get to, from town markers and link ID
				var fromMarker = townsMarkers[data[i].from_town],
					toMarker = townsMarkers[data[i].to_town],
					id = data[i].link_id;

				//parse coordinates into float values
				var fromX = parseFloat(fromMarker.getLatLng().lng),
					fromY = parseFloat(fromMarker.getLatLng().lat),
					toX = parseFloat(toMarker.getLatLng().lng),
					toY = parseFloat(toMarker.getLatLng().lat);

				//weed out results with bad coordinates
				if(!isNaN(fromX) && !isNaN(fromY) && !isNaN(toX) && !isNaN(toY)) {
					//create a new GeoJSON feature
					var newFeature = {
						"type": "Feature",
						"geometry": {
							"type": "LineString",
							"coordinates": [[fromX, fromY], [toX, toY]]
						},
						"properties": {
							"id": id
						}
					}
					//add feature to collection
					linkCollection.push(newFeature);
				}
			}

			//add collection to layer
			linkLayerGroup.addData(linkCollection);

			//if town marker has already been plotted
			if(townPlotted) {
				townsMarkerGroup.bringToFront();
			} else {
				//gets town marker for selected FER, styles
				$.get('/town/getByRegion/' + region, function(town){
					townMarker = townsMarkers[town.town_id];
					townMarker.setStyle({
						color: '#485558',
						fillOpacity: 1,
						opacity: 0.5,
						weight: 10
					});
					townMarker.setRadius(7);
					townPlotted = true; //we've plotted our town marker
					townsMarkerGroup.addLayer(townMarker);
					townsMarkerGroup.bringToFront();
				});
			}

			//spinner.stop();

			//get details for each link and render to detail area
			//getLinkDetails();

			//get industries for the selected and partner regions
			//populates drop down box for user selection
			//getIndustries();

		});
	});
}

/**
 * Handles link feature events, styles link features
 **/
function handleLink(feature, layer) {
	layer.on({
		mouseover: enterLink
	});

	layer.setStyle({
		color: '#26b0cc',
		opacity: 1,
		weight: 4
	});

	//geneate arrowheads for link lines
	var arrowHead = L.polylineDecorator(layer);
	linkLayerGroup.addLayer(arrowHead);

	var offset = '100%',
		repeat = '0%';

	arrowHead.setPatterns([
		{
			offset: offset, 
			repeat: repeat, 
			symbol: L.Symbol.arrowHead({
				pixelSize: 17, 
				polygon: false, 
				pathOptions: {
					stroke: true,
					color: '#26b0cc'
				}
			})
		}
	]);

	//bind popup to link
	$.get('link/get/' + feature.properties.id, function(data){
		var from = data['from_region'],
			to = data['to_region'],
			popup = L.popup()
					.setContent(
						'<a href="#" class="link-popup" data-id="' + 
						feature.properties.id + '">' + from + ' to ' + 
						to + '</a>'
					);
		linkPopups[feature.properties.id] = popup;
	});
}

/**
 * Event function for entering link features
 **/
function enterLink(e) {
	var feature = e.target.feature,
		popup = linkPopups[feature.properties.id];

	popup.setLatLng(e.latlng).openOn(linkedMap);
}


$(document).on('click', '.link-popup', function(e){
	e.preventDefault();
	$.get('/link/useLink/' + $(this).data('id'), function(data){
		refreshLinks();
	});
});

/**
 * Change event for link direction drop-down,
 * sets link direction in session.
 **/
$(document).on('change', '#link-direction', function(){
	var val = $(this).val();
	$.post('/link/setDirection', {direction: val}, function(data){
		refreshLinks();
	});
});

/**
 * Gets details of links for selected region(s) and
 * populates right detail area of map
 **/
function getLinkDetails() {
	 $('.linked-details').slimScroll({
        height: '100%',
        width: '300px'
    });

	$.get('/link/getDetails', function(data) {
		$('.linked-details').html(data);
	});
}

/**
 * Change event for industry drop-down box, changes selected
 * industry in session.
 **/
$(document).on('change', '.industry-select select', function(){
	var val = $(this).val();
	$.post('/link/setIndustry', {industry: val}, function(data){
		getContent('linked'); //update map
	});
});

/**
 * Change event for commodity drop-down box, changes selected
 * commodity in session.
 **/
$(document).on('change', '.commodity-select select', function(){
	var val = $(this).val();
	$.post('/link/setCommodity', {commodity: val}, function(data){
		getContent('linked'); //update map
	});
});

/**
 * Initializes flow range slider
 **/
function flowInit() {
	$.get('/link/getFlowVals', function(data) {
		var flowSlider = $(".flow-select #flow-range").slider({
	        range: true,
	        min: data.flowVals.min,
	        max: data.flowVals.max,
	        values: [ data.flow.min, data.flow.max ],
	        slide: function( event, ui ) {
	        	$('.flow-select #flow-min').html(ui.values[0]);
	   			$('.flow-select #flow-max').html(ui.values[1]);
	        },
	        stop: function( event, ui ) {
	        	$.post('/link/setFlowVals', {
	   				min: ui.values[0],
	   				max: ui.values[1]
	   			}, function(data) {
	   				refreshLinks();
	   			})
	        }
	    });
	    $('.flow-select .flow-range-min').html(flowSlider.slider("option", "min"));
	    $('.flow-select .flow-range-max').html(flowSlider.slider("option", "max"));
	    $('.flow-select #flow-min').html(flowSlider.slider("option", "values")[0]);
	    $('.flow-select #flow-max').html(flowSlider.slider("option", "values")[1]);
	});
}

function partnerMapInitialize() {
	partnerMap = L.map('partner-map', {
		zoomControl: false,
		scrollWheelZoom: false
	}).fitBounds([
		[60.149787, -74.100890],
		[43.703738, -51.425110]
	]);

	L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.{ext}', {
		attribution: '',
		subdomains: 'abcd',
		minZoom: 0,
		maxZoom: 20,
		ext: 'png',
		opacity: 0.3
	}).addTo(partnerMap);

	L.control.zoom({
		position:'bottomleft'
	}).addTo(partnerMap);

	$.get('/region/getPartner', function(data){
		partner = data.id;
		boundMap(partnerMap, data.prov);
		//Adds TopoJSON data to map
		$.getJSON('js/bnd.json')
		  .done(addTopoData);

		function addTopoData(topoData){ 
	  		partnerTopoLayer = new L.TopoJSON(topoData, {
			    onEachFeature: handlePartnerFeature
			}).addTo(partnerMap);
		}
	});
}

function handlePartnerFeature(feature, layer) {
	layer.on({
		mouseover: enterPartnerFeature,
		mouseout: leavePartnerFeature,
		click: clickPartnerFeature
	});

	layer.setStyle({
		fillColor: '#fff',
		fillOpacity: 0.3,
		color: '#485558',
		weight: 1
	});

	$.get('/region/getPartner', function(data){
		if(feature.id == data.id) {
			layer.setStyle({
	            fillColor: '#eed16d'
	        });
		}
	});
}

function enterPartnerFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 2,
		fillOpacity: 0.5
	});
}

function leavePartnerFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 1,
		fillOpacity: 0.3
	});
}

function clickPartnerFeature(e) {
	partnerTopoLayer.setStyle({
		fillColor: '#fff',
		fillOpacity: 0.3
	});

	var layer = e.target;
	$.post('/region/setPartner', {id: layer.feature.id}, function(data) {
		//set fill colour to blue
		layer.setStyle({
			fillColor: '#eed16d'
		});

		boundMap(partnerMap, data.prov);
		$('.side-prov select').val(data.prov);

		console.log(data);

		$.get('/region/getPartnerByProv/' + data.prov + '/' + layer.feature.id, function(data){
			$('.partner-select-inputs').html(data);
			refreshLinks();
		});
	});
}

$(document).on('change', '.partner-select .side-fer select', function(e) {
	var val = $(this).val();
	$.post('/region/setPartner', {id: val}, function(data) {
		$('#link-direction').prop('disabled', (val != 'all'));

		$.get('/region/getPartnerByProv/' + data.prov + '/' + val, function(data){
			$('.partner-select-inputs').html(data);
			partnerTopoLayer.setStyle({
				fillColor: '#fff',
				fillOpacity: 0.3
			});
			partnerTopoLayer.eachLayer(function(layer) {  
				if(layer.feature.id == val) {    
					layer.setStyle({
						fillColor: '#eed16d'
					});
				}
			});
			refreshLinks();	
		});
	});
});

$(document).on('change', '.partner-select .side-prov select', function(e){
	var val = $(this).val();
	$.post('/province/setPartner', {prov: val}, function(data){
		$.get('/region/getPartnerByProv/' + val, function(data){
			$('.partner-select-inputs').html(data);
			partnerTopoLayer.setStyle({
				fillColor: '#fff',
				fillOpacity: 0.3
			});
			refreshLinks();
			//boundMap(partnerMap, val);
			//$('#link-direction').removeAttr('disabled');
		});
	});
});

function refreshLinks() {
	getSidebar('linked', function(){
		getContent('linked');
	});
}