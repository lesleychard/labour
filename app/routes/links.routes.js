module.exports = function(app) {
	var link = require('../controllers/links.controller');
	app.get('/link/getDirection', link.getDirection);
	app.get('/link/getLinks', link.getLinks);
	app.post('/link/setDirection', link.setDirection);
	app.get('/link/get/:id', link.get);
	app.post('/link/setIndustry', link.setIndustry);
	app.get('/link/getDetails', link.getDetails);
	app.post('/link/setCommodity', link.setCommodity);
	app.get('/link/useLink/:id', link.useLink);
	app.get('/link/getFlowVals', link.getFlowVals);
	app.post('/link/setFlowVals', link.setFlowVals);
}