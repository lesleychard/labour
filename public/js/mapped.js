var info, province, map, topoLayer, selectedRegion, regionPopups = [];

function mapInitialize() {
	//Initializes map object
	map = L.map('map-area', {
		zoomControl: false
	}).setView([53,-59.0625], 5);

	//Add zoom control to bottom left
	L.control.zoom({
	     position:'bottomleft'
	}).addTo(map);

	//Basemap, etc
	L.tileLayer('http://a{s}.acetate.geoiq.com/tiles/acetate-base/{z}/{x}/{y}.png', {
		maxZoom: 18,
		attribution: 'Atlantic Labour Force',
		id: 'examples.map-20v6611k'
	}).addTo(map);

	//get selected FER from session
	$.get('/region/getSelected', function(data){
		selectedRegion = data.id;
		boundMap(map, data.prov);
		//Adds TopoJSON data to map
		$.getJSON('js/bnd.json')
		  .done(addTopoData);

		function addTopoData(topoData){  
	  		topoLayer = new L.TopoJSON(topoData, {
			    onEachFeature: handleFeature
			}).addTo(map);
		}
	});

	info = L.control();

	info.onAdd = function(map) {
	    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
	    this.update();
	    return this._div;
	};

	// method that we will use to update the control based on feature properties passed
	info.update = function(data) {
	    this._div.innerHTML = (data ? data : 'Hover over a region to see more information.');
	};

	info.addTo(map);

	province = L.control({position: 'topleft'});

	province.onAdd = function(map) {
		//create DOM elements
		this._div = L.DomUtil.create('div', 'province');
		this._select = L.DomUtil.create('select', 'province-select', this._div);

		//stores possible province options to fill select
		var options = [
			{name: 'All Atlantic', key: 0},
			{name: 'Newfoundland & Labrador', key: 1},
			{name: 'Prince Edward Island', key: 2},
			{name: 'Nova Scotia', key: 3},
			{name: 'New Brunswick', key: 4}			
		];

        this._select.innerHTML = "";

		for(var i = 0; i < options.length; i++) {
			this._select.innerHTML += '<option value="' + options[i].key +
								'" id="option-' + options[i].key + '">' + 
								options[i].name + '</option>';
		}

		this.update();
		return this._div;
	}

	province.update = function(props) {
		$.get('/province/getSelected', function(data){
			boundMap(map, data.prov);
			$('.province-select #option-' + data.prov).attr('selected', 'selected');
		})
	}

	province.addTo(map);
}

function handleFeature(feature, layer) {
	layer.on({
		mouseover: enterFeature,
		mouseout: leaveFeature,
		click: clickFeature
	});

	layer.setStyle({
		fillColor: '#39e6ac',
		fillOpacity: 1,
		color: '#485558',
		weight: 1
	});

	if(feature.id == selectedRegion) {
		layer.setStyle({
            fillColor: '#26b0cc'
        });
	}
}

function enterFeature(e) {
	var layer = e.target;

	layer.setStyle({
		weight: 2,
		opacity: 1
	});

	$.get('/region/getPopup/' + layer.feature.id, function(data){
		info.update(data);
	});
}

function leaveFeature(e) {
	var layer = e.target
	layer.setStyle({
		weight: 1
	});
	info.update();
}

function clickFeature(e) {
	//set all polygons to teal
	topoLayer.setStyle({
		fillColor: '#39e6ac'
	});

	var layer = e.target;
	//set region as selected
	$.post('/region/set', {id: layer.feature.id}, function(data){
		$('.list-graphed').addClass('list-active');
		changeNav('graphed');
		getContent('graphed');
		getSidebar('graphed');
	});
}

$(document).on('change', '.province-select', function(e){
	e.preventDefault();
	var val = $(this).val();
	$.post('/province/set', {prov: val}, function(data){
		boundMap(map, val);
		//set all polygons to teal
		topoLayer.setStyle({
			fillColor: '#39e6ac'
		});
		$('.province-select option').removeAttr('selected');
		$('.province-select #option-' + val).attr('selected', 'selected');
	})
})

function percentStats() {
	$('.percentage-stat').each(function(){
		var i = 360 * (parseInt($(this).data('perc'))/100);  
			var background = "linear-gradient(" + (i-90) + "deg, transparent 50%, #26b0cc 50%),linear-gradient(-90deg, #26b0cc 50%, transparent 50%)";
		$(this).css({
			"background-image": background,
		});
	});
}