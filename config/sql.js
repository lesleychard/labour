var mysql = require('mysql');
var config = require('./database-dev.json');

var connection = mysql.createConnection({
  host     : config.host,
  user     : config.user,
  password : config.password,
  database : config.database
});

var queues = require('mysql-queues');
const DEBUG = true;
queues(connection, DEBUG);

connection.execute = function(query, callback) {
	this.query(query, function(err, results) {
		if(err) {
			console.log(err.error);
			console.log(query);
		}
		callback(results);
	});
}

module.exports = connection;