var links = require('../models/links.model.js');
var regions = require('../models/regions.model.js');
var provinces = require('../models/provinces.model.js');

var link = { 

	getDirection : function(req, res) {
		var direction = req.session.direction;
		if(direction == undefined) direction = req.session.direction = "from";
		res.send(direction);
	},

	setDirection : function(req, res) {
		var direction = req.session.direction = req.body.direction,
			flow = req.session.flow = null;
		res.send(direction);
	},

	getLinks : function(req, res) {
		var region = req.session.region,
			prov = req.session.prov,
			direction = req.session.direction,
			partner = req.session.partner,
			partnerProv = req.session.partnerProv,
			industry = req.session.linkIndustry,
			commodity = req.session.linkCommodity,
			flow = req.session.flow = null;
		
		links.findLinks(region, prov, direction, partner, partnerProv, industry, commodity, flow, function(linkRes){
			res.send(linkRes);
		});
	},

	get : function(req, res) {
		var linkId = req.params.id;
		links.findById(linkId, function(linkRes){
			res.send(linkRes[0]);
		});
	},

	setIndustry : function(req, res) {
		var industry = req.session.linkIndustry = req.body.industry;
		if(industry == 'all') industry = req.session.linkIndustry = null;
		res.send(industry);
	},

	setCommodity : function(req, res) {
		var commodity = req.session.linkCommodity = req.body.commodity;
		if(commodity == 'all') commodity = req.session.linkCommodity = null;
		res.send(commodity);
	},

	getDetails : function(req, res) {
		var region = req.session.region,
			prov = req.session.prov,
			direction = req.session.direction,
			partner = req.session.partner,
			partnerProv = req.session.partnerProv,
			industry = req.session.linkIndustry,
			commodity = req.session.linkCommodity,
			flow = req.session.flow;

		if(direction == undefined) direction = req.session.direction = "from";

		links.findDetails(region, prov, partner, partnerProv, direction, industry, commodity, flow, function(linkRes){
			regions.findNames([region, partner], [prov, partnerProv], function(regRes) {
				//if(region != null && partner != null) direction = "and";
				res.render('links/details', {
					region: regRes['region_name'],
					direction: direction,
					partner: regRes['partner_name'],
					links: linkRes,
					noLinks: (linkRes.length == 0),
				});
			});
		});
	},

	useLink : function(req, res, next) {
		var id = req.params.id;
		links.findById(id, function(linkRes){
			var region = req.session.region = linkRes[0]['from_region_id'],
				province = req.session.prov = linkRes[0]['from_prov'],
				partner = req.session.partner = linkRes[0]['to_region_id'],
				partnerProv = req.session.partnerProv = linkRes[0]['to_prov'];
			res.send(linkRes);
			next();
		});
	},

	getFlowVals : function(req, res) {
		var region = req.session.region,
			province = req.session.prov,
			partner = req.session.partner,
			partnerProv = req.session.partnerProv,
			direction = req.session.direction,
			industry = req.session.linkIndustry,
			commodity = req.session.linkCommodity,
			flow = req.session.flow;
		links.findFlowVals(region, province, partner, partnerProv, direction, industry, commodity, function(flows){
			if(!flow) {
				flow = req.session.flow = {
					min: flows[0].min,
					max: flows[0].max
				}
			}
			res.send({
				flowVals: flows[0],
				flow: flow
			});
		});
	},

	setFlowVals : function(req, res) {
		var	flow = req.session.flow = req.body;
		res.send(req.session.flow);
	}
}

module.exports = link;