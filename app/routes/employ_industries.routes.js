module.exports = function(app) {
    var employIndustry = require('../controllers/employ_industries.controller');
    app.get('/employInd/getById/:id', employIndustry.getById);
    app.get('/employInd/getOccupationChart/:id', employIndustry.getOccupationChart);
}