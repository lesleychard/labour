var employIndustries = require('../models/employ_industries.model.js');
var employIndustryOccupations = require('../models/employ_industry_occupations.model.js');

var employIndustry = {
	getById : function(req,res) {
		var id = req.params.id;
		employIndustries.findById(id, function(indRes){
			res.send(indRes[0]);
		});
	},
	getOccupationChart : function(req, res) {
		var industry = req.params.id;

		employIndustries.findById(industry, function(indRes){
			if(indRes[0] != undefined) {
				var indCode = indRes[0].three_code;
				employIndustryOccupations.findByInd(indCode, false, function(occs){
					var occsList = [];
					for(var i = 0; i < occs.length; i++) {
						occsList.push({
							y: occs[i].convert_ratio,
							name: occs[i].name
						})
					}
					res.send(occsList);
				});
			}
		});
	}
}

module.exports = employIndustry;