var appRoot = require('app-root-path');
var conn = require(appRoot + '/config/sql.js');

var links = {

	findLinks : function(region, province, direction, partner, partnerProv, industry, commodity, flow, callback) {
		var query = 'SELECT links.*, tw1.town_id AS to_town, tw2.town_id AS from_town FROM links ' +
					'JOIN commodities ON links.commodity_id = commodities.commodity_id ' +
					'JOIN industries ON links.industry_id = industries.industry_id ' + 
					'JOIN towns tw1 ON links.to_region_id = tw1.region_id ' +
					'JOIN towns tw2 ON links.from_region_id = tw2.region_id ';
		var where = false,
			oppositeDirection;

		if(direction == "to") {
			oppositeDirection = "from";
		} else {
			oppositeDirection = "to";
		}

		if(region == null && province != 0) query += 'JOIN regions t1 ON links.' + direction + '_region_id = t1.region_id ';
		if(partner == null && partnerProv != 0) query += 'JOIN regions t2 ON links.' + oppositeDirection + '_region_id = t2.region_id ';

		if(region == null) {
			if(province != 0) {
				query += 'WHERE t1.province_id = ' + province;
				where = true;
			}
		} else {
			query += 'WHERE links.' + direction + '_region_id = ' + region;
			where = true;
		}

		if(partner == null) {
			if(partnerProv != 0) {
				if(where) {
					query += ' AND ';
				} else {
					query += 'WHERE ';
					where = true;
				}
				query += 't2.province_id = ' + partnerProv;
				
			}
		} else {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}

			if(region != null) {
				query += '((links.' + direction + '_region_id = ' + region + ' AND ' +
						 'links.' + oppositeDirection + '_region_id = ' + partner + ') OR (' + 
						 'links.' + oppositeDirection + '_region_id = ' + region + ' AND ' +
						 'links.' + direction + '_region_id = ' + partner + '))';
			} else {
				query += 'links.' + oppositeDirection + '_region_id = ' + partner;
			}
		}

		if(industry) {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			query += 'links.industry_id = ' + industry; 
		}

		if(commodity) {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			query += 'links.commodity_id = ' + commodity; 
		}

		if(flow && flow.min != null) {
			if(where) {
				query += ' AND (';
			} else {
				query += 'WHERE (';
				where = true;
			}
			query += 'links.flow >= ' + flow.min + ' AND links.flow <= ' + flow.max + ')';
		}

		if(where) {
			query += ' AND (';
		} else {
			query += 'WHERE (';
			where = true;
		}
		query += 'links.flow > 2) ';

		if(region == null) {
			query += ' GROUP BY links.' + direction + '_region_id'; 
		} else {
			query += ' GROUP BY links.' + oppositeDirection + '_region_id'; 
		}


		//console.log(query);
		conn.execute(query, callback);
	},

	findById : function(id, callback) {
		var query = 'SELECT links.*, r1.name AS from_region, r2.name AS to_region, ' +
					'r1.province_id AS from_prov, r2.province_id AS to_prov ' +
					'FROM links ' + 
					'JOIN regions r1 ON links.from_region_id = r1.region_id ' +
					'JOIN regions r2 ON links.to_region_id = r2.region_id ' +
					'WHERE links.link_id = ' + id;
		conn.execute(query, callback);
	},

	findIndustries : function(region, province, partner, partnerProv, direction, callback) {
		var query = 'SELECT industries.name AS name, industries.industry_id AS id FROM links ' +
					'JOIN industries ON links.industry_id = industries.industry_id ';
		var where = false,
			oppositeDirection;

		if(direction == "to") {
			oppositeDirection = "from";
		} else {
			oppositeDirection = "to";
		}

		if(region == null && province != 0) query += 'JOIN regions t1 ON links.' + direction + '_region_id = t1.region_id ';
		if(partner == null && partnerProv != 0) query += 'JOIN regions t2 ON links.' + oppositeDirection + '_region_id = t2.region_id ';

		if(region == null) {
			if(province != 0) {
				query += 'WHERE t1.province_id = ' + province;
				where = true;
			}
		} else {
			query += 'WHERE links.' + direction + '_region_id = ' + region;
			where = true;
		}

		if(partner == null) {
			if(partnerProv != 0) {
				if(where) {
					query += ' AND ';
				} else {
					query += 'WHERE ';
					where = true;
				}
				query += 't2.province_id = ' + partnerProv;
				
			}
		} else {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			if(region != null) {
				query += '((links.' + direction + '_region_id = ' + region + ' AND ' +
						 'links.' + oppositeDirection + '_region_id = ' + partner + ') OR (' + 
						 'links.' + oppositeDirection + '_region_id = ' + region + ' AND ' +
						 'links.' + direction + '_region_id = ' + partner + '))';
			} else {
				query += 'links.' + oppositeDirection + '_region_id = ' + partner;
			}
		}

		if(where) {
			query += ' AND (';
		} else {
			query += 'WHERE (';
			where = true;
		}
		query += 'links.flow > 2) ';

		query += ' GROUP BY industries.name ORDER BY industries.name'; 

		//console.log(query);
		conn.execute(query, callback);
	},

	findCommodities : function(region, province, partner, partnerProv, direction, callback) {
		var query = 'SELECT commodities.name AS name, commodities.commodity_id AS id FROM links ' +
					'JOIN commodities ON links.commodity_id = commodities.commodity_id ';
		var where = false,
			oppositeDirection;

		if(direction == "to") {
			oppositeDirection = "from";
		} else {
			oppositeDirection = "to";
		}

		if(region == null && province != 0) query += 'JOIN regions t1 ON links.' + direction + '_region_id = t1.region_id ';
		if(partner == null && partnerProv != 0) query += 'JOIN regions t2 ON links.' + oppositeDirection + '_region_id = t2.region_id ';

		if(region == null) {
			if(province != 0) {
				query += 'WHERE t1.province_id = ' + province;
				where = true;
			}
		} else {
			query += 'WHERE links.' + direction + '_region_id = ' + region;
			where = true;
		}

		if(partner == null) {
			if(partnerProv != 0) {
				if(where) {
					query += ' AND ';
				} else {
					query += 'WHERE ';
					where = true;
				}
				query += 't2.province_id = ' + partnerProv;
				
			}
		} else {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			if(region != null) {
				query += '(links.' + direction + '_region_id = ' + region + ' AND ' +
						 'links.' + oppositeDirection + '_region_id = ' + partner + ')';
			} else {
				query += 'links.' + oppositeDirection + '_region_id = ' + partner;
			}
		}

		if(where) {
			query += ' AND (';
		} else {
			query += 'WHERE (';
			where = true;
		}
		query += 'links.flow > 2) ';

		query += ' GROUP BY commodities.name ORDER BY commodities.name'; 

		//console.log(query);
		conn.execute(query, callback);
	},

	findDetails: function(region, province, partner, partnerProv, direction, industry, commodity, flow, callback) {
		var where = false,
			oppositeDirection;

		if(direction == "to") {
			oppositeDirection = "from";
		} else {
			oppositeDirection = "to";
		}

		var query = 'SELECT links.flow AS flow, ' +
					'industries.name AS industry_name, ' +
					'commodities.name AS commodity_name, ' +
					't1.name AS ' + direction + '_region, ' +
					't2.name AS ' + oppositeDirection + '_region ' +
					'FROM links ' +
					'JOIN commodities ON links.commodity_id = commodities.commodity_id ' +
					'JOIN industries ON links.industry_id = industries.industry_id ' +
					'JOIN regions t1 ON links.' + direction + '_region_id = t1.region_id ' +
					'JOIN regions t2 ON links.' + oppositeDirection + '_region_id = t2.region_id ';

		if(region == null) {
			if(province != 0) {
				query += 'WHERE t1.province_id = ' + province;
				where = true;
			}
		} else {
			if(partner == null) {
				query += 'WHERE links.' + direction + '_region_id = ' + region;
				where = true;
			}
		}

		if(partner == null) {
			if(partnerProv != 0) {
				if(where) {
					query += ' AND ';
				} else {
					query += 'WHERE ';
					where = true;
				}
				query += 't2.province_id = ' + partnerProv;
				
			}
		} else {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			if(region != null) {
				query += '(links.' + direction + '_region_id = ' + region + ' AND ' +
						 'links.' + oppositeDirection + '_region_id = ' + partner + ')';
			} else {
				query += 'links.' + oppositeDirection + '_region_id = ' + partner;
			}
		}

		if(industry) {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			query += 'links.industry_id = ' + industry; 
		}

		if(commodity) {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			query += 'links.commodity_id = ' + commodity; 
		}

		/*if(flow) {
			if(where) {
				query += ' AND (';
			} else {
				query += 'WHERE (';
				where = true;
			}
			query += 'links.flow >= ' + flow.min + ' AND links.flow <= ' + flow.max + ')';
		}*/

		if(where) {
			query += ' AND (';
		} else {
			query += 'WHERE (';
			where = true;
		}
		query += 'links.flow > 2) ';

		//console.log(query);
		conn.execute(query, callback);
	},

	findFlowVals : function(region, province, partner, partnerProv, direction, industry, commodity, callback) {
		var query = 'SELECT MIN(links.flow) AS min, MAX(links.flow) AS max FROM links ';
		var where = false,
			oppositeDirection;

		if(direction == "to") {
			oppositeDirection = "from";
		} else {
			oppositeDirection = "to";
		}

		if(region == null && province != 0) query += 'JOIN regions t1 ON links.' + direction + '_region_id = t1.region_id ';
		if(partner == null && partnerProv != 0) query += 'JOIN regions t2 ON links.' + oppositeDirection + '_region_id = t2.region_id ';

		if(region == null) {
			if(province != 0) {
				query += 'WHERE t1.province_id = ' + province;
				where = true;
			}
		} else {
			query += 'WHERE links.' + direction + '_region_id = ' + region;
			where = true;
		}

		if(partner == null) {
			if(partnerProv != 0) {
				if(where) {
					query += ' AND ';
				} else {
					query += 'WHERE ';
					where = true;
				}
				query += 't2.province_id = ' + partnerProv;
				
			}
		} else {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			if(region != null) {
				query += '((links.' + direction + '_region_id = ' + region + ' AND ' +
						 'links.' + oppositeDirection + '_region_id = ' + partner + ') OR (' + 
						 'links.' + oppositeDirection + '_region_id = ' + region + ' AND ' +
						 'links.' + direction + '_region_id = ' + partner + '))';
			} else {
				query += 'links.' + oppositeDirection + '_region_id = ' + partner;
			}
		}

		if(industry) {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			query += 'links.industry_id = ' + industry; 
		}

		if(commodity) {
			if(where) {
				query += ' AND ';
			} else {
				query += 'WHERE ';
				where = true;
			}
			query += 'links.commodity_id = ' + commodity; 
		}

		//console.log(query);
		conn.execute(query, callback);
	}

}

module.exports = links;