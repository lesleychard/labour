var express = require('express');
var cookieParser = require('cookie-parser');
var session = require('cookie-session');
var hbs = require('hbs');
var bodyParser = require('body-parser');
var restart = true;

module.exports = function() {
    var app = express();

    app.set('views', './app/views');
    app.set('view engine', 'hbs');
    hbs.registerPartials('./app/views/partials');

    app.use(cookieParser());
    app.use(session({secret: '1234567890QWERTY'}));

    app.use( bodyParser.json() );
    app.use(bodyParser.urlencoded({
      extended: true
    }));

    app.use(function(req, res, next){
        if(restart) {
            req.session = {};
            req.session.intros = {
                'mapped': true,
                'graphed': true,
                'tabular': true,
                'linked': true
            };
        }
        restart = false;
        next();
    });

    require('../app/routes/index.routes.js')(app);
    require('../app/routes/provinces.routes.js')(app);
    require('../app/routes/regions.routes.js')(app);
    require('../app/routes/occupations.routes.js')(app);
    require('../app/routes/demographies.routes.js')(app);
    require('../app/routes/employment.routes.js')(app);
    require('../app/routes/links.routes.js')(app);
    require('../app/routes/towns.routes.js')(app);
    require('../app/routes/employ_industries_occupations.routes.js')(app);
    require('../app/routes/employ_industries.routes.js')(app);

    app.use(express.static('./public'));
    return app;
};