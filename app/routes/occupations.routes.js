module.exports = function(app) {
    var occupation = require('../controllers/occupations.controller');
    app.get('/occupation/typeAhead/:key', occupation.typeAhead);
    app.post('/occupation/set', occupation.set);
    app.post('/occupation/unset', occupation.unset);
    app.post('/occupation/setTag', occupation.setTag);
    app.post('/occupation/unsetTag', occupation.unsetTag);
    app.get('/occupation/getFiltered', occupation.getFiltered);
    app.get('/occupation/getFilterOptions', occupation.getFilterOptions);
    app.post('/occupation/setFilterOptions', occupation.setFilterOptions);
    app.get('/occupation/getSkills', occupation.getSkills);
    app.get('/occupation/getReplPot/:id', occupation.getReplPot);
    app.get('/occupation/getReplPotChart/:id', occupation.getReplPotChart);
};