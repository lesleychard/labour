var provinces = require('../models/provinces.model.js');

var province = {
	get : function(req, res) {
		var id = req.params.id;
		provinces.findById(id, function(provRes){
			res.send(provRes);
		})
	},
	getAll : function(req, res) {
		provinces.findAll(function(provRes){
			res.send(provRes);
		});
	},
	getSelected : function(req, res) {
		res.send({prov: req.session.prov});
	},
	set : function(req, res) {
		var prov = req.session.prov = req.body.prov,
			region = req.session.region = null,
			flow = req.session.flow = null;
		res.send(prov);
	},
	setPartner : function(req, res) {
		var partnerProv = req.session.partnerProv = req.body.prov,
			partner = req.session.partner = null,
			flow = req.session.flow = null;
		res.send(partnerProv);
	}
}

module.exports = province;