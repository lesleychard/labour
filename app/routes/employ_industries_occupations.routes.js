module.exports = function(app) {
    var employIndustryOccupation = require('../controllers/employ_industry_occupations.controller');
    app.get('/employIndOcc/getByInd/:id', employIndustryOccupation.getByInd);
}