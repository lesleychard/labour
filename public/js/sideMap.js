var sideMap, sideTopoLayer, selectedFer, partnerMap, partnerTopoLayer, partnerFer;

//Loads TopoJSON data like GeoJSON
L.TopoJSON = L.GeoJSON.extend({  
  addData: function(jsonData) {    
    if (jsonData.type === "Topology") {
      for (key in jsonData.objects) {
        geojson = topojson.feature(jsonData, jsonData.objects[key]);
        L.GeoJSON.prototype.addData.call(this, geojson);
      }
    }    
    else {
      L.GeoJSON.prototype.addData.call(this, jsonData);
    }
  }  
});

$(document).on('change', '.side-fer select', function(e) {
	e.preventDefault();
	var parent = $(this).parent().parent(),
		val = $(this).val(),
		select = '';

	if(parent.hasClass('partner-select-inputs')) select = 'Partner';

	$.post('/fer/select' + select, {name: val}, function(data){
		$.get('/fer/get' + select + 'Id', function(data){
			if(parent.hasClass('partner-select-inputs')) {
				partnerTopoLayer.setStyle({
					fillColor: '#fff',
					fillOpacity: 0.3
				});
				partnerTopoLayer.eachLayer(function(layer) {  
				  if(layer.feature.id == data.id) {    
				    layer.setStyle({
				    	fillColor: '#26b0cc'
				    });
				  }
				});
				toggleDirection(val.substring(0,3) == 'all');

			} else {
				sideTopoLayer.setStyle({
					fillColor: '#fff',
					fillOpacity: 0.3
				});
				sideTopoLayer.eachLayer(function(layer) {  
				  if(layer.feature.id == data.id) {    
				    layer.setStyle({
				    	fillColor: '#26b0cc'
				    });
				  }
				});
			}
			getContent($('.list-nav .list-active a').html());
			getIndustries();
			flowInit();
		});
	});
});

$(document).on('change', '.side-prov select', function(e){
	e.preventDefault();
	var parent = $(this).parent().parent(),
		prov = $(this).val(),
		partner = false,
		map = sideMap,
		layer = sideTopoLayer;


	if(parent.hasClass('partner-select-inputs')) {
		partner = true;
		map = partnerMap;
		layer = partnerTopoLayer;
	}

	$.post('/graphed/provFers/' + prov, {ferSelected: false, partnerFer: partner}, function(data){
		boundMap(map, prov);
		layer.setStyle({
			fillColor: '#fff',
			fillOpacity: 0.3
		});
		if(prov == 'all') {
			$('.side-fer').fadeOut('fast');
		} else {
			$('.side-fer select').html(data);
			$('.side-fer').fadeIn('fast')
		}
		getContent($('.list-nav .list-active a').html());
		toggleDirection(true);
		getIndustries();
		flowInit();
	});
});

function hideSelectAll() {
	$.get('/tabular/prov', function(data){
		if(data == 'all') $('.side-fer').fadeOut('fast');
	});
}

function sideMapInitialize() {
	sideMap = L.map('side-map', {
		zoomControl: false
	}).fitBounds([
		[60.149787, -74.100890],
		[43.703738, -51.425110]
	]);

	L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.{ext}', {
		attribution: '',
		subdomains: 'abcd',
		minZoom: 0,
		maxZoom: 20,
		ext: 'png',
		opacity: 0.3
	}).addTo(sideMap);

	$.get('/tabular/prov', function(data) {
		boundMap(sideMap, data);
	});

	$.get('/fer/getId', function(data){
		selectedFer = data.id;
		//Adds TopoJSON data to map
		$.getJSON('js/bnd.json')
		  .done(addTopoData);

		function addTopoData(topoData){ 
	  		sideTopoLayer = new L.TopoJSON(topoData, {
			    onEachFeature: handleSideFeature
			}).addTo(sideMap);
		}
	});
}

function handleSideFeature(feature, layer) {
	layer.on({
		mouseover: enterSideFeature,
		mouseout: leaveSideFeature,
		click: clickSideFeature
	});

	layer.setStyle({
		fillColor: '#fff',
		fillOpacity: 0.3,
		color: '#485558',
		weight: 1
	});

	if(feature.id == selectedFer) {
		layer.setStyle({
            fillColor: '#26b0cc'
        });
	}
}

function enterSideFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 2,
		fillOpacity: 0.5
	});
}

function leaveSideFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 1,
		fillOpacity: 0.3
	});
}

function clickSideFeature(e) {
	sideTopoLayer.setStyle({
		fillColor: '#fff',
		fillOpacity: 0.3
	});
	var layer = e.target;
	$.get('/fer/get/' + layer.feature.id, function(data) {
		$.post('/fer/select', {name: data[0].FIRST_THRE}, function(prov){
			//set fill colour to blue
			layer.setStyle({
				fillColor: '#26b0cc'
			});
			boundMap(sideMap, prov);
			$('.side-prov select').val(prov);
			getContent($('.list-nav .list-active a').html());
			$.post('/graphed/provFers/' + prov, {ferSelected: true, partnerFer: false}, function(data){
				if(prov == 'all') {
					$('.side-fer').fadeOut('fast');
				} else {
					$('.side-fer select').html(data);
					$('.side-fer').fadeIn('fast')
				}
			});
		});
	});
}

function partnerMapInitialize() {
	partnerMap = L.map('partner-map', {
		zoomControl: false,
		scrollWheelZoom: false
	}).fitBounds([
		[60.149787, -74.100890],
		[43.703738, -51.425110]
	]);

	L.control.zoom({
	     position:'bottomleft'
	}).addTo(partnerMap);

	L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.{ext}', {
		attribution: '',
		subdomains: 'abcd',
		minZoom: 0,
		maxZoom: 20,
		ext: 'png',
		opacity: 0.3
	}).addTo(partnerMap);

	$.get('/fer/getPartnerProv', function(data) {
		boundMap(partnerMap, data);
	});

	$.get('/fer/getPartnerId', function(data){
		partnerFer = data.id;
		//Adds TopoJSON data to map
		$.getJSON('js/bnd.json')
		  .done(addTopoData);

		function addTopoData(topoData){ 
	  		partnerTopoLayer = new L.TopoJSON(topoData, {
			    onEachFeature: handlePartnerFeature
			}).addTo(partnerMap);
		}
	});
}

function handlePartnerFeature(feature, layer) {
	layer.on({
		mouseover: enterPartnerFeature,
		mouseout: leavePartnerFeature,
		click: clickPartnerFeature
	});

	layer.setStyle({
		fillColor: '#fff',
		fillOpacity: 0.3,
		color: '#485558',
		weight: 1
	});

	if(feature.id == partnerFer) {
		layer.setStyle({
            fillColor: '#eed16d'
        });
	}
}

function enterPartnerFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 2,
		fillOpacity: 0.5
	});
}

function leavePartnerFeature(e) {
	var layer = e.target;
	layer.setStyle({
		weight: 1,
		fillOpacity: 0.3
	});
}

function clickPartnerFeature(e) {
	partnerTopoLayer.setStyle({
		fillColor: '#fff',
		fillOpacity: 0.3
	});
	var layer = e.target;
	$.get('/fer/get/' + layer.feature.id, function(data) {
		$.post('/fer/selectPartner', {name: data[0].FIRST_THRE}, function(prov){
			//set fill colour to yellow
			layer.setStyle({
				fillColor: '#eed16d'
			});
			boundMap(partnerMap, prov);
			$('.side-prov select').val(prov);
			$.post('/graphed/provFers/' + prov, {ferSelected: true, partnerFer: true}, function(data){
				if(prov == 'all') {
					$('.side-fer').fadeOut('fast');
				} else {
					$('.side-fer select').html(data);
					$('.side-fer').fadeIn('fast')
				}
			});
			getContent('linked');
			toggleDirection(false);
			getIndustries();
			flowInit();
		});
	});
}

function toggleDirection(isOn) {
	if(isOn) {
		$('.side-select').removeClass('disabled');
		$('.side-select select').prop('disabled', false);
	} else {
		$('.side-select').addClass('disabled');
		$('.side-select select').prop('disabled', true);
	}
}
