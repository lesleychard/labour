module.exports = function(app) {
    var index = require('../controllers/index.controller');
    app.get('/', index.main);
    app.post('/setNav', index.setNav);
    app.get('/mapped', index.mapped);
    app.get('/mapped/side', index.mappedSide);
    app.get('/tabular', index.tabular);
    app.get('/tabular/side', index.tabularSide);
    app.get('/graphed', index.graphed);
    app.get('/graphed/side', index.graphedSide);
    app.get('/linked', index.linked);
    app.get('/linked/side', index.linkedSide);
};