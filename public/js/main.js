var spinner = {
    spin: function() {
        $('#spinner').addClass('active');
    },
    stop: function() {
        $('#spinner').removeClass('active');
    }
}

/**
 * Bind click event to nav menu
 * Sets selected nav item, gets main content, gets sidebar content
 **/
$(document).on('click', '.list-nav > li > a', function(e){
	e.preventDefault();
    //set active nav element on frontend
	$('.list-active').removeClass('list-active');
	$(this).parent().addClass('list-active');

    var targetName = $(this).html();
    //set nav session var
    changeNav(targetName);
    //render content and sidebar for selected nav item
    getSidebar(targetName, getContent(targetName));
});

/**
 * Sets nav session variable
 **/
function changeNav(target) {
    $.post('/setNav', {name: target}, function(data){
        $('.list-nav').html(data);
    });
}

$('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

$('body').popover({
    selector: '[data-toggle="popover"]',
    html: true
});

/**
 * When document has loaded, get content and sidebar for selected nav
 **/
$(document).ready(function(){
    var val = $('.list-nav .list-active a').html();
    getContent(val);
    getSidebar(val);
    $('nav .nav-container').slimScroll({
        height: '100%'
    });
});

/**
 * Gets main content for selected nav item
 **/
function getContent(target) {
    $.get('/' + target, function(data){
        $('.col-content').html(data);
        switch(target) {
            case 'linked':
                linkedMapInitialize();
                $.get('/region/getSelected', function(region){
                    console.log('reg id:' + region.id);
                    if(region.id != null) {
                        getLinks();
                        getLinkDetails();
                    }
                });
                var intro = $('#intro-linked').val();
                if(intro == "true") {
                    startLinkedIntro();
                }
                break;
            case 'mapped':
                mapInitialize();
                var intro = $('#intro-mapped').val();
                if(intro == "true") {
                    $('#introModal').modal({
                        show: true
                    });
                }
                break;
            case 'tabular':
                scrollInits();
                var intro = $('#intro-tabular').val();
                if(intro == "true") {
                    startTabularIntro();
                }
                break;
            case 'graphed':
                demographyChart();
                var id = $('#industry-select select').val();
                employmentChart(id);
                getTopOccupations(id);
                getOccupationChart(id)
                var intro = $('#intro-graphed').val();
                if(intro == "true") {
                    startGraphedIntro();
                }
                break;
        }
        callback();
    });
}

/**
 * Get sidebar content for selected nav item
 **/
function getSidebar(target, callback) {
    $.get('/' + target + '/side', function(data){
        $('#sidebar-content').html(data);
        switch(target) {
            case 'tabular':
                sideMapInitialize();
                skillChart();
                break;
            case 'graphed':
                graphedMapInitialize();
                break;
            case 'linked' :
                partnerMapInitialize();
                flowInit();
                break;
            case 'mapped' :
                percentStats();
                break;
        }
        callback();
    });
}

function boundMap(map, prov) {
    //stores bounding box for each province
    var bounds = [
        {
            min : [60.149787, -74.100890],
            max : [43.703738, -51.425110]
        },
        {
            min : [56.604950, -66.893859],
            max : [46.502180, -51.995165]
        },
        {
            min : [47.066489, -64.630675],
            max : [45.878648, -62.147765]
        },
        {
            min : [47.312765, -67.046433],
            max : [43.377112, -59.729539]
        },
        {
            min : [48.111105, -69.353562],
            max : [45.263295, -63.904344]
        }
    ];
    //sets the min and max bounds for selected province
    map.fitBounds([
        bounds[prov].min,
        bounds[prov].max
    ]);
}

//Loads TopoJSON data like GeoJSON
L.TopoJSON = L.GeoJSON.extend({  
  addData: function(jsonData) {    
    if (jsonData.type === "Topology") {
      for (key in jsonData.objects) {
        geojson = topojson.feature(jsonData, jsonData.objects[key]);
        L.GeoJSON.prototype.addData.call(this, geojson);
      }
    }    
    else {
      L.GeoJSON.prototype.addData.call(this, jsonData);
    }
  }  
});