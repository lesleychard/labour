var employment = require('../models/employment.model.js');

var employ = {
	getChart : function(req, res) {
		var prov = req.session.prov,
			region = req.session.region,
			industry = req.session.industry = req.params.id;

		employment.findSeries(region, prov, industry, function(emRes){
			var series = [];
			for(var i = 0; i < emRes.length; i++){
				var d = Date.UTC(emRes[i]['year'].substring(0,4), emRes[i]['year'].substring(5, 7));
				series.push([
					d, emRes[i]['number_employees']
				]);
			}
			if(emRes[emRes.length - 1]['year'].substring(0,4) != "2013") {
				series.push([
					Date.now(), 0
				]);
			}
			res.send(series);
		});
	}
}

module.exports = employ;
