var appRoot = require('app-root-path');
var conn = require(appRoot + '/config/sql.js');

var regionOccupation = {
	findTop : function(region, prov, limit, callback) {
		var query;
		if(region == null) {
			query = 'SELECT SUM(prov_occupations.volume) AS volume, ' +
					'occupations.name AS name, ' +
					'occupations.occupation_id AS occupation_id ' +
					'FROM prov_occupations ' +
					'JOIN occupations ON prov_occupations.occupation_id = occupations.occupation_id ';
			if(prov != 0) query += 'WHERE prov_occupations.province_id = ' + prov;
			query += ' GROUP BY occupations.name ORDER BY volume DESC LIMIT ' + limit;
		} else {
			query = 'SELECT SUM(region_occupations.volume) AS volume, ' +
					'occupations.name AS name, ' +
					'occupations.occupation_id AS occupation_id ' +
					'FROM region_occupations ' +
					'JOIN occupations ON region_occupations.occupation_id = occupations.occupation_id ' +
					'WHERE region_occupations.region_id = ' + region +
					' GROUP BY occupations.name ORDER BY volume DESC LIMIT ' + limit;
		}
		conn.execute(query, callback);
	}
}

module.exports = regionOccupation;