var appRoot = require('app-root-path');
var conn = require(appRoot + '/config/sql.js');

var region = {

	findAll : function(callback) {
		var query = 'SELECT * FROM regions';
		conn.execute(query, callback);
	},

	findById : function(id, callback) {
		var query = 'SELECT * FROM regions WHERE region_id = ' + id;
		conn.execute(query, callback);
	},

	findWithProv : function(id, prov, callback) {
		var query = 'SELECT ';
		if(id == null) {
			query += 'name FROM provinces WHERE province_id = ' + prov;
		} else {
			query += '* FROM regions WHERE region_id = ' + id;
		}
		conn.execute(query, callback);
	},

	findByProv : function(prov, callback) {
		var query = 'SELECT regions.region_id AS region_id, ' +
					'regions.name AS region_name, ' +
					'provinces.abbr AS prov_abbr, ' +
					'provinces.name AS prov_name ' +
					'FROM regions JOIN provinces ON ' +
					'regions.province_id = provinces.province_id ' +
					'WHERE regions.province_id = ' + prov;
		conn.execute(query, callback);
	},

	findNames : function(names, provs, callback) {
		var q = conn.startTransaction(),
			namesRes = [],
			types = ['region', 'partner'];

		if(names[0] == null) {
			var query = 'SELECT name FROM provinces WHERE province_id = ' + provs[0];
		} else {
			var query = 'SELECT name FROM regions WHERE region_id = ' + names[0];
		}

		q.query(query, function(err, results) {
			if(err) console.log(err.error);
			namesRes['region_name'] = results[0].name;
		});

		if(names[1] == null) {
			var query2 = 'SELECT name FROM provinces WHERE province_id = ' + provs[1];
		} else {
			var query2 = 'SELECT name FROM regions WHERE region_id = ' + names[1];
		}

		q.query(query2, function(err, results) {
			if(err) console.log(err.error);
			namesRes['partner_name'] = results[0].name;
		});
		
		q.commit(function(err, results){
			if(err) console.log(err.error);
			callback(namesRes);
		});
	},

	findPopupDetails: function(id, callback) {
		var working = '', notWorking = '';

		for(var i = 0; i < 85; i = i+5) {
			if(i > 14 && i < 65) {
				if(i != 15) working += ' + ';
				working += 'demographies.pop_' + i + '_' + (i+4);
			} else {
				notWorking += ' + demographies.pop_' + i + '_' + (i+4);
			}
		}
		notWorking += ' + demographies.pop_85';

		var query = 'SELECT ' +
					'regions.name AS name, ' +
					'regions.pop_2011 AS population, ';

		query += 'ROUND(((' + working + ')/demographies.pop_total)*100, 2) AS percent_working, ' +
				 'ROUND(((' + notWorking + ')/(' + working + ')*100), 2) AS depend_ratio ' + 
				 'FROM regions JOIN demographies ON demographies.region_id = regions.region_id ' + 
				 'WHERE regions.region_id = ' + id;
		conn.execute(query, callback);
	}
	
}

module.exports = region;