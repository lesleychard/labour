var occupations = require('../models/occupations.model.js');

var occupation = {

	typeAhead : function(req, res) {
		var key = req.params.key;
		occupations.findByKey(key, function(occRes){
			res.send(occRes);
		});
	},

	set : function(req, res, next) {
		var id = parseInt(req.body.id),
			occupationIds = req.session.occupationIds;

		if(!occupationIds) occupationIds = req.session.occupationIds = [];

		if(occupationIds.indexOf(id) < 0) occupationIds.push(id);

		occupations.findByIds(occupationIds, function(occRes){
			res.send(occRes);
			next();
		});		
	},

	unset : function(req, res, next) {
		var id = parseInt(req.body.id),
			occupationIds = req.session.occupationIds;

		for(var i = 0; i < occupationIds.length; i++){
			if(occupationIds[i] == id) {
				occupationIds.splice(i, 1);
			}
		}

		res.send(occupationIds);
		next();
	},

	setTag : function(req, res, next) {
		var tag = req.body.tag,
			occupationTags = req.session.occupationTags;

		if(!occupationTags) occupationTags = req.session.occupationTags = [];

		if(occupationTags.indexOf(tag) < 0) occupationTags.push(tag);

		res.send(occupationTags);
		next();
	},

	unsetTag : function(req, res, next) {
		var tag = req.body.tag,
			occupationTags = req.session.occupationTags,
			n = occupationTags.indexOf(tag);
		
		occupationTags.splice(n, 1);
			
		res.send(occupationTags);
		next();
	},

	getFiltered : function(req, res) {
		var provId = req.session.prov,
			occupationIds = req.session.occupationIds,
			occupationTags = req.session.occupationTags,
			filterOptions = req.session.filterOptions;

		if(!provId) provId = 0;

		occupations.filter(occupationIds, occupationTags, filterOptions, provId, function(occupationRes){
			var occupationNum = occupationRes.length;
			res.render('occupations/getFiltered', {
				resultNum: occupationNum,
				occupations: occupationRes,
				isAll: (provId == 0)
			});
		});
	},

	getFilterOptions : function(req, res) {
		var filterOptions = req.session.filterOptions;
		if(filterOptions == undefined || filterOptions.toString() == "") {
			filterOptions = req.session.filterOptions = {
				skill_level : [
					{name: '0', active: false},
					{name: 'A', active: false},
					{name: 'B', active: false},
					{name: 'C', active: false},
					{name: 'D', active: false}
				],
				wage_attr : [
					{name: 'Non-Competitive', active: false},
					{name: 'Marginally Non-Competitive', active: false},
					{name: 'Marginally Competitive', active: false},
					{name: 'Competitive', active: false}
				],
				repl_pot : [
					{name: 'Very Low', active: false},
					{name: 'Low', active: false},
					{name: 'High', active: false},
					{name: 'Very High', active: false}
				]
			}
		}
		res.render('occupations/getFilterOptions', {
			skill: filterOptions.skill_level,
			wageattr: filterOptions.wage_attr,
			replpot: filterOptions.repl_pot
		});
	},

	setFilterOptions : function(req, res, next) {
		var optionType = req.body.optionType;
			optionValue = req.body.optionValue;
			filterOptions = req.session.filterOptions;

		var active = filterOptions[optionType][optionValue].active
		filterOptions[optionType][optionValue].active = !active;

		res.send(filterOptions);
		next();
	},

	getSkills : function(req, res) {
		var skills = [];
		occupations.countSkills(function(skillRes){
			skills.push(skillRes[0]);
			if(skills.length == 5) {
				res.send(skills);
			}
		});
	},

	getReplPot : function(req, res) {
		var id = req.params.id;
		occupations.findById(id, function(occRes){
			res.render('occupations/getReplPot', {
				name: occRes[0].name
			});
		});
	},

	getReplPotChart : function(req, res) {
		var id = req.params.id;
		occupations.findReplPot(id, function(occRes){
			var series = [],
				replCodes = {
					'N/A' : 0,
					'Very Low' : 1,
					'Low' : 2,
					'High' : 3,
					'Very High' : 4
				},
				colorCodes = {
					'Very Low' : '#d7191c',
					'Low' : '#fdae61',
					'High' : '#a6d96a',
					'Very High' : '#1a9641'
				};

			for(var i = 0; i < occRes.length; i++) {
				series.push({
					x: occRes[i].province_id,
					y: replCodes[occRes[i]['repl_pot']],
					name: occRes[i].province,
					color: colorCodes[occRes[i]['repl_pot']]
				});
			}
			res.send({
				series: series,
				name: occRes[0].name
			});
		});
	}
};

module.exports = occupation;