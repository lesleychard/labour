var appRoot = require('app-root-path');
var conn = require(appRoot + '/config/sql.js');

var employIndustryOccupation = {
	findByInd : function(industry, limit, callback) {
		var query = 'SELECT SUM(employ_industry_occupations.convert_ratio) AS convert_ratio, ' +
					'employ_industry_occupations.occupation_id AS occupation_id, ' + 
					'occupations.name AS name, ' + 
					'occupations.skill_level AS skill_level ' +
					'FROM employ_industry_occupations ' +
					'JOIN occupations ON employ_industry_occupations.occupation_id = occupations.occupation_id ' +
					'WHERE employ_industry_occupations.employ_ind_three = ' + industry + 
					' AND employ_industry_occupations.convert_ratio > 0.01 ' +
					' GROUP BY employ_industry_occupations.occupation_id ' +
					'ORDER BY convert_ratio DESC';
		if(limit) query += ' LIMIT ' + limit;
		conn.execute(query, callback);
	}
}

module.exports = employIndustryOccupation;