module.exports = function(app) {
    var employment = require('../controllers/employment.controller');
    app.get('/employment/getChart/:id', employment.getChart);
}