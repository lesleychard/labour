function startMappedIntro(){

  var intro = introJs();
  intro.setOptions({
    showProgress: true,
    showBullets: false,
    steps: [
      {
        element: '#step1',
        intro: "This is the application's <strong>sidebar</strong>.<br /><br />Here is where you will find the <strong>main navigation</strong> as well additional information relevant to your exploration.<br /><br />There's more information here beyond the fold.<br /><br />When your mouse hovers over this area, a scroll bar will appear.<br /><br />You can <strong>scroll through the sidebar</strong> by dragging the scroll bar or using the scroll wheel or area on your mouse.",
        position: 'right',
        highlightClass: 'step-1-highlight',
        tooltipClass: 'step-1-tooltip',
      },
      {
        element: '#step2',
        intro: "For your convenience, the data tools are divided into <strong>four tabs</strong>: map overview (the current category), infographics, tabular data, and trade links.<br /><br />By clicking on one of these tabs, you will be able to access a variety of exploration tools which analyze distinct economic and geographic phenomena.",
        position: 'right',
        tooltipClass: 'step-2-tooltip'
      },
      {
        element: '#step3',
        intro: "In the map overview tab, the sidebar is where you will find <strong>overview statistics</strong> on the region you have selected to analyze.",
        position: 'right',
        highlightClass: 'step-3-highlight',
        tooltipClass: 'step-3-tooltip'
      },
      {
        element: '#step4',
        intro: "This is the main map pane.<br /><br />Here you'll be able to <strong>select a province or functional economic region</strong> by using the province drop-down box on the top left, or by clicking on an economic region on the map.<br /><br />As you <strong>hover over each economic region</strong>, more information will be available about the respective geography and economy.",
        position: 'left',
        highlightClass: 'step-4-highlight',
        tooltipClass: 'step-4-tooltip'
      }
    ]
  });

  intro.onafterchange(function(target) {
    switch (target.getAttribute('id')) {
      case "step1":
        $('.step-1-tooltip, .step-2-tooltip').parent().height($(window).height());
        $('.step-1-highlight').height($(window).height());
        $('.step-1-tooltip, .step-2-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '10px',
          left: '10px'
        });
        break;
      case "step2":
        $('.step-2-tooltip').parent().height($(window).height());
        $('.step-1-tooltip, .step-3-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-10px',
          left: '10px'
        });
        break;
      case "step3":
        $('.step-2-tooltip, .step-4-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-20px',
          left: '-10px'
        });
        break;
      case "step4":
        $('.introjs-tooltipReferenceLayer').css({
          height: ($(window).height() - 10) + 'px',
          width: parseInt($(window).width() * 0.75) + 'px'
        });
        $('.step-4-highlight').css({
          height: ($(window).height() - 10) + 'px',
          width: parseInt($(window).width() * 0.75) + 'px'
        });
        $('.introjs-helperNumberLayer').css('top', '50px');
        break;
    }
  });

  intro.onbeforechange(function(target){
    switch (target.getAttribute('id')) {
      case "step3":
        $('nav').css('margin-top', '-250px');
        $('nav').css('height', $(window).height() + 250);
        break;
      case "step2":
      case "step4":
        $('nav').css('margin-top', '0px');
        $('nav').css('height', '100%');
        break;
    }
  });

  intro.onexit(function(){
    $('nav').css('margin-top', '0px');
    $('nav').css('height', '100%');
  });

  intro.start();
}

$(document).on('click', '#intro-start', function(){
  $('#introModal').modal('hide');
  startMappedIntro();
});

function startGraphedIntro() {
  var intro = introJs();
  intro.setOptions({
    showProgress: true,
    showBullets: false,
    steps: [
      {
        intro: "Would you like some help using the infographic tools?<br /><br />Proceed with the tutorial by clicking 'Next' or hit 'Skip' if you'd like to start exploring.",
        tooltipClass: 'step-1-tooltip'
      },
      {
        element: '#demography-chart',
        intro: "The charts tab is where you'll get information on the <strong>demography and employment</strong> of the region.<br /><br />The first graph you'll find in the main content area is a column graph of the selected regions demography.<br /><br />Here, you will find not only the actual population in each age cohort, but the ideal population given a low growth curve.",
        position: 'left',
        tooltipClass: 'step-2-tooltip'
      },
      {
        element: '#graphed-step2',
        intro: "Here are some <strong>population statistics</strong> from the selected region.<br /><br />To view more in-depth information about a statistic, click the '?' icon.",
        position: 'left',
        tooltipClass: 'step-3-tooltip'
      },
      {
        element: "#graphed-step3",
        intro: "These are the <strong>top industries</strong> of the selected region by employment.<br /><br />By clicking on one of these industries, you will be able to view a time-series of employment in the selected region (to the right), as well as view a break-down of occupations involved in the industry (below).",
        position: 'right'
      },
      {
        element: '#industry-chart',
        intro: "This series shows the <strong>number of people employed</strong> in the chosen industry from the selected region over time.",
        position: 'left',
        tooltipClass: 'step-4-tooltip'
      },
      {
        element: '#industry',
        intro: "In addition to clicking on the top industries, industries can be chosen using this drop-down menu.",
        position: 'left',
        tooltipClass: 'step-5-tooltip'
      },
      {
        element: '#top-ind-occupations',
        intro: "In the occupation breakdown, you will see the <strong>top occupations</strong> by percentage of employees for the selected industry.<br /><br />Clicking on any occupation will bring you to the tabular data page to inspect the occupation further.",
        position: 'left',
        tooltipClass: 'step-6-tooltip'
      },
      {
        element: '#occupations-chart',
        intro: "The <strong>occupation breakdown chart</strong> shows the composition of occupations in the selected industry.<br /><br />Hover over any slice on this chart to view more information.",
        position: 'right',
        tooltipClass: 'step-7-tooltip'
      },
      {
        element: '#graphed-step8',
        intro: "You can <strong>change the province or economic region</strong> you're viewing by using this drop-down box or clicking on an area in the provided map.",
        position: 'right',
        tooltipClass: 'step-8-tooltip'
      },
      {
        element: '#graphed-step9',
        intro: "The sidebar in this section shows you the <strong>top occupations overall</strong> for the selected region.",
        position: 'right',
        tooltipClass: 'step-9-tooltip'
      }
    ]
  });

  intro.onafterchange(function(target){
    switch (target.getAttribute('id')) {
      case "demography-chart":
        $('.step-1-tooltip, .step-3-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-10px',
          left: '-10px'
        });
        break;
      case "industry-chart":
        $('.step-5-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-10px',
          left: '-10px'
        });
        break;
      case "industry":
        $('.step-4-tooltip, .step-6-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-30px',
          left: '-10px'
        });
        break;
      case "top-ind-occupations":
        $('.step-7-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-30px',
          left: '-10px'
        });
        break;
      case "occupations-chart":
        $('.step-6-tooltip, .step-8-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-10px',
          left: '-10px'
        });
        $('#graphed').css({
          height: 'auto',
          overflow: 'visible'
        });
        break;
      case "graphed-step8":
        $('#graphed').css({
          height: $(window).height() + 'px',
          overflow: 'hidden'
        });
        $('.step-7-tooltip, .step-9-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-20px',
          left: '5px'
        });
        break;
    }
  });

  intro.onbeforechange(function(target){
    switch (target.getAttribute('id')) {
      case "graphed-step9":
        $('nav').css('margin-top', '-350px');
        $('nav').css('height', $(window).height() + 350);
        break;
      case "graphed-step8":
        $('nav').css('margin-top', '0px');
        $('nav').css('height', '100%');
        break;
    }
  });

  intro.onexit(function(){
    $('nav').css('margin-top', '0px');
    $('nav').css('height', '100%');
  });

  intro.oncomplete(function(){
    $('nav').css('margin-top', '0px');
    $('nav').css('height', '100%');
  });

  intro.start();
}

function startTabularIntro() {
  var intro = introJs();
  $('.content-wrapper').css({
    overflow: 'hidden',
    height: $(window).height() + 'px'
  });

  intro.setOptions({
    showProgress: true,
    showBullets: false,
    steps: [
      {
        intro: "We can help you get started here, too!<br /><br />Click 'Next' if you'd like to start the tutorial, or hit 'Skip' to keep exploring.",
        tooltipClass: 'step-1-tooltip'
      },
      {
        element: '#tabular-step2',
        intro: "The <strong>tabular data tools</strong> are most useful for exploring data surrounding occupations.<br /><br />Here, you'll find statistics and facts about <strong>hundreds of occupations</strong> in the selected region.<br /><br />See how an occupation shapes up Atlantic-wide by clicking on its name in this list.<br /><br />Need more explanation? Clicking on any of the '?' icons will give you more detailed descriptions of the stats included.",
        tooltipClass: 'step-2-tooltip',
        highlightClass: 'step-2-highlight',
        position: 'left'
      },
      {
        element: '#filter-occupations',
        intro: "Type in this field to <strong>search occupations</strong>.<br /><br />As soon as you start typing, you'll see a list of recorded occupations below.",
        tooltipClass: 'step-3-tooltip',
        position: 'right'
      },
      {
        element: '#filter-tags-form',
        intro: "The occupation search box allows you to <strong>search specific occupations</strong> (by clicking items in the drop-down list) or use <strong>general search terms</strong> by typing and hitting the enter button on your keyboard.<br /><br />All search terms will appear to the right, and can be removed by clicking on their respective 'X'.",
        tooltipClass: 'step-4-tooltip',
        highlightClass: 'step-4-highlight',
        position: 'right'
      },
      {
        element: '#filter-options',
        intro: "Want to <strong>filter your results</strong> based on skill level, wage attraction, or replacement potential?<br /><br />Click this button to view more options.",
        tooltipClass: 'step-4-tooltip',
        position: 'right'
      },
      {
        element: '#filter-options-list',
        intro: "<strong>Click</strong> on one of these filters for it to be <strong>applied to the current search</strong>.<br /><br />Once selected, simply click it again to remove.",
        position: 'right',
        tooltipClass: 'step-5-tooltip'
      },
      {
        element: '#tabular-step6',
        intro: "The <strong>selected province</strong> can be changed at any time from the sidebar.",
        position: 'right',
        tooltipClass: 'step-6-tooltip'
      },
      {
        element: '#skills-chart',
        intro: "Here you can see how the <strong>skill levels</strong> are distributed over the selected region.",
        position: 'right',
        tooltipClass: 'step-7-tooltip'
      }
    ]
  });

  intro.onafterchange(function(target){
    switch (target.getAttribute('id')) {
      case "tabular-step2":
        $('.step-2-highlight').css('width', (parseInt($(window).width() * 0.75) - 10) + 'px');
        $('.introjs-tooltipReferenceLayer').css('width', (parseInt($(window).width() * 0.75) - 10) + 'px');
        $('.step-1-tooltip, .step-3-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-10px',
          left: '-10px'
        });
        break;
      case "filter-occupations":
        $('.step-2-tooltip, .step-4-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-10px',
          left: '-30px'
        });
        $('#filter-occupations').val("");
        showFilterOptions("");
        break;
      case "filter-tags-form":
        $('.step-3-tooltip, .step-5-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-10px',
          left: '-30px'
        });

        $("#filter-occupations").typed({
          strings: ["manager"],
          typeSpeed: 10
        });

       setTimeout(
        function() {
          $('#filter-occupations').val("manager");
          showFilterOptions("manager");
          $('.step-4-highlight').height(320);
        }, 450);

        break;
      case "filter-options":
        $('#filter-occupations').val("");
        showFilterOptions("");
         $('#filter-options').removeClass('active');
        break;
      case "tabular-step6":
        $('.step-5-tooltip, .step-7-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-20px',
          left: '10px'
        });
        $('#filter-options').removeClass('active');
        break;
    }
  });

  intro.onbeforechange(function(target){
    switch (target.getAttribute('id')) {
      case "filter-options-list":
        $('#filter-options').addClass('active');
        $('.step-6-tooltip, .step-5-tooltip, .step-4-tooltip').parent().children('.introjs-helperNumberLayer').css({
          top: '-20px',
          left: '-10px'
        });
        filterOptions();
        break;
      case "tabular-step6":
        $('nav').css('margin-top', '0px');
        $('nav').css('height', '100%');
        break;
      case "skills-chart":
        $('nav').css('margin-top', '-170px');
        $('nav').css('height', $(window).height() + 170);
        break;
    }
  });

  intro.onexit(function(){
    showFilterOptions("");
    $('#filter-occupations').val("");
    $('#filter-options').removeClass('active');
    $('.content-wrapper').css({
      overflow: 'auto',
      height: 'auto'
    });
    $('nav').css('margin-top', '0px');
    $('nav').css('height', '100%');
  });

  intro.oncomplete(function(){
    $('.content-wrapper').css({
      overflow: 'auto',
      height: 'auto'
    });
    $('nav').css('margin-top', '0px');
    $('nav').css('height', '100%');
  });

  intro.start();
}

var started = false;

function startLinkedIntro() {
  var intro = introJs();

  intro.setOptions({
    showProgress: true,
    showBullets: false,
    steps: [
      {
        intro: "Would you like some help getting started using the links tools?"
      },
      {
        element: "#linked-map",
        intro: "Before you start exploring trade links, you will need to <strong>click on a region</strong> to select it.<br /><br />As you hover over places on this map, the respective region name will appear in the upper-left corner.",
        position: 'left',
        highlightClass: 'step-2-highlight'
      },
      {
        element: "#step4",
        intro: "You are now viewing all the <strong>trade links for the selected region</strong>.<br /><br />By using your mouse, you can <strong>hover over trade links</strong> (arrows on the map) to view the involved cities.<br /><br /><strong>Clicking on the names</strong> of the two cities will select the two involved regions, and display all of the links between the two regions.",
        position: 'left',
        highlightClass: 'step-3-linked-highlight'
      },
      {
        element: "#linked-details-container",
        intro: "This area displays details about the trade links involving the selected region(s).<br /><br /><strong>Flow values</strong> are denoted by a small blue arrow accompanied by an integer value.<br /><br />These values represent the amount of dollars exchanged in the trade, where <strong>1 unit = $1,000,000 CAD</strong>.",
        position: 'left',
        highlightClass: 'step-4-linked-highlight'
      },
      {
        element: "#linked-step5",
        intro: "Use this drop-down box to <strong>filter the direction</strong> of the trade links, to or from your selected region.",
        position: 'right',
        highlightClass: 'step-5-linked-highlight'
      },
      {
        element: '#linked-step6',
        intro: "Would you like to analyze trade <strong>between two specific regions?</strong><br /><br />Use the provided map or drop-down boxes to <strong>select a partner province or partner region</strong>.",
        position: 'right',
        highlightClass: 'step-6-linked-highlight'
      },
      {
        element: '#linked-step7',
        intro: "You can <strong>filter trade links</strong> by the type of commodity or industry involved.<br /><br />Commodities are a product or service being sold to an industry in the target region.",
        position: 'right',
        highlightClass: 'step-7-linked-highlight'
      }
    ],
    disableInteraction: false
  });

  $(document).on('click', '.leaflet-interactive', function(){
    started = true;
    intro.exit();
  });

  intro.onafterchange(function(target){
    switch (target.getAttribute('id')) {
        case "linked-map":
          $('.introjs-overlay').css('z-index', 5);
          $('.introjs-tooltipReferenceLayer').css('z-index', 10);
          $('.introjs-helperNumberLayer').css({
            top: '40px',
            left: '-40px'
          });
          $('.introjs-tooltipReferenceLayer').css({
            height: ($(window).height() - 10) + 'px',
            width: parseInt($(window).width() * 0.75) + 'px'
          });
          $('.step-2-highlight').css({
            height: ($(window).height() - 10) + 'px',
            width: parseInt($(window).width() * 0.75) + 'px'
          });
          $('.introjs-nextbutton').hide();
          break;
        case "step4":
          $('.introjs-tooltipReferenceLayer').css({
            height: ($(window).height() - 10) + 'px',
            width: parseInt($(window).width() * 0.75) + 'px'
          });
          $('.step-3-linked-highlight').css({
            height: ($(window).height() - 10) + 'px',
            width: parseInt($(window).width() * 0.75) + 'px'
          });
          $('.introjs-helperNumberLayer').css({
            top: '40px',
            left: '-40px'
          });
          $('.introjs-nextbutton').show();
          break;
        case "linked-details-container":
          $('.introjs-tooltipReferenceLayer').css({
            height: ($(window).height() - 10) + 'px',
            left: ($(window).width() - 300) + 'px',
            width: '300px'
          });
           $('.step-4-linked-highlight').css({
            height: ($(window).height() - 10) + 'px',
            left: ($(window).width() - 300) + 'px',
            width: '300px'
          });
          break;
        case "linked-step5":
          $('.introjs-helperNumberLayer').css({
            top: '-24px',
            left: '-22px'
          });
          break;
        case "linked-step6":
          $('.introjs-helperNumberLayer').css({
            top: '-24px',
            left: '5px'
          });
          break;
        case "linked-step7":
          $('.introjs-helperNumberLayer').css({
            top: '-24px',
            left: 'auto',
            right: '5px'
          });
          break;
        default:
          $('.introjs-overlay').css('z-index', 999999);
          $('.introjs-tooltipReferenceLayer').css('z-index', 10000000);
          $('.introjs-nextbutton').show();
          break;
    }
  });

  intro.onbeforechange(function(target){
    switch (target.getAttribute('id')) {
      case "linked-step5":
        $('nav').css('margin-top', '0px');
        $('nav').css('height', '100%');
        break;
      case "linked-step6":
        $('nav').css('margin-top', '-200px');
        $('nav').css('height', $(window).height() + 200);
        break;
      case "linked-step7":
          $('nav').css('margin-top', '-350px');
          $('nav').css('height', $(window).height() + 350);
          break;
    }
  });

  intro.onexit(function(){
    $('nav').css('margin-top', '0px');
    $('nav').css('height', '100%');
  });

  intro.oncomplete(function(){
    $('nav').css('margin-top', '0px');
    $('nav').css('height', '100%');
  });

  intro.start();
  if($('#linked-details-container').length > 0 && started) {
    intro.goToStep(3);
  }
}