var regions = require('../models/regions.model.js');
var provinces = require('../models/provinces.model.js');
var employment = require('../models/employment.model.js');

var region = {
	get : function(req, res) {
		var id = req.params.id;
		regions.findById(id, function(regRes){
			res.send(regRes[0]);
		});
	},
	getSelected : function(req, res) {
		var region = req.session.region;
		var prov = req.session.prov;
		if(typeof region != 'number') {
			region = null;
		}
		if(prov == undefined) {
			prov = 0;
		}
		res.send({
			id: region,
			prov: prov
		});
	},
	set : function(req, res, next) {
		var id = req.body.id,
			flow = req.session.flow = null;

		if(id == 'all') {
			var region = req.session.region = null,
				prov = req.session.prov;
			res.send({prov: prov});
			next();
		} else {
			var region = req.session.region = parseInt(id);
			regions.findById(id, function(regRes) {
				var prov = req.session.prov = regRes[0]['province_id'];
				res.send({prov: prov});
				next();
			});
		}
	},
	getByProv : function(req, res, next) {
		var prov = req.params.prov,
			region = req.session.region;
		if(req.params.prov) prov = req.params.prov;
		if(req.params.id) region = req.params.id;

		regions.findByProv(prov, function(regRes){
			var regList = [],
				provList = [];

			if(prov != 0) {
				regList.push({
					id: 'all',
					name: 'All ' + regRes[0]['prov_name'],
					selected: (region == null)
				});
			}

			for(var i = 0; i < regRes.length; i++) {
				regList.push({
					id: regRes[i]['region_id'],
					name: regRes[i]['region_name'],
					selected: (regRes[i]['region_id'] == region)
				});
			}

			provinces.findAll(function(provRes){
				for(var i = 0; i < provRes.length; i++) {
					provList.push({
						id : provRes[i]['province_id'],
						name : provRes[i]['abbr'],
						selected : (provRes[i]['province_id'] == prov)
					});
				}

				res.render('regions/getByProv', {
					regions: regList,
					provs: provList
				});
			});
		});
	},

	getPartnerByProv : function(req, res) {
		var prov = req.params.partnerProv,
			partner = req.session.partner;
		if(req.params.prov) prov = req.params.prov;
		if(req.params.id) partner = req.params.id;

		regions.findByProv(prov, function(regRes){
			var regList = [],
				provList = [];

			if(prov != 0) {
				regList.push({
					id: 'all',
					name: 'All ' + regRes[0]['prov_name'],
					selected: (partner == null)
				});
			}

			for(var i = 0; i < regRes.length; i++) {
				regList.push({
					id: regRes[i]['region_id'],
					name: regRes[i]['region_name'],
					selected: (regRes[i]['region_id'] == partner)
				});
			}

			provinces.findAll(function(provRes){
				provList.push({
					id: 0,
					name: 'ALL',
					selected: (prov == 0)
				});

				for(var i = 0; i < provRes.length; i++) {
					provList.push({
						id : provRes[i]['province_id'],
						name : provRes[i]['abbr'],
						selected : (provRes[i]['province_id'] == prov)
					});
				}

				res.render('regions/getByProv', {
					regions: regList,
					provs: provList,
					isAll: (prov == 0)
				});
			});
		});
	},

	getPartner : function(req, res) {
		var partner = req.session.partner;
		var partnerProv = req.session.partnerProv;
		if(typeof partner != 'number') {
			partner = null;
		}
		if(partnerProv == undefined) {
			partnerProv = 0;
		}
		res.send({
			id: partner,
			prov: partnerProv
		});
	},

	setPartner : function(req, res, next) {
		var id = req.body.id,
			flow = req.session.flow = null;

		if(id == 'all') {
			var partner = req.session.partner = null;
			var partnerProv = req.session.partnerProv;
			res.send({prov: partnerProv});
			next();
		} else {
			var partner = req.session.partner = parseInt(id);
			regions.findById(id, function(regRes) {
				var partnerProv = req.session.partnerProv = regRes[0]['province_id'];
				res.send({prov: partnerProv});
				next();
			});
		}
	},

	getPopup : function(req, res) {
		var id = req.params.id;
		regions.findPopupDetails(id, function(regRes){
			employment.findTopIndustries(id, null, 3, function(topInd){
				res.render('regions/getPopup', {
					region: regRes[0],
					topInd: topInd
				});
			});
		});
	}
}

module.exports = region;