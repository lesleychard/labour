module.exports = function(app) {
    var province = require('../controllers/provinces.controller');
    app.get('/province/get/:id', province.get);
    app.get('/province/getAll', province.getAll);
    app.get('/province/getSelected', province.getSelected);
    app.post('/province/set', province.set);
    app.post('/province/setPartner', province.setPartner);
};