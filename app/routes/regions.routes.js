module.exports = function(app) {
    var region = require('../controllers/regions.controller');
    app.get('/region/get/:id', region.get);
    app.get('/region/getSelected', region.getSelected);
    app.post('/region/set', region.set);
    app.get('/region/getByProv/:prov', region.getByProv);
    app.get('/region/getByProv/:prov/:id', region.getByProv);
    app.get('/region/getPartnerByProv/:prov', region.getPartnerByProv);
    app.get('/region/getPartnerByProv/:prov/:id', region.getPartnerByProv);
    app.get('/region/getPartner', region.getPartner);
    app.post('/region/setPartner', region.setPartner);
    app.get('/region/getPopup/:id', region.getPopup);
};