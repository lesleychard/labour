module.exports = function(app) {
	var demography = require('../controllers/demographies.controller');
	app.get('/demography/getChart', demography.getChart);
};