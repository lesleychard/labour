var towns = require('../models/towns.model.js');

var town = {
	getByRegion : function(req, res) {
		var region = req.params.region;
		towns.findByRegion(region, function(townRes){
			res.send(townRes[0]);
		})
	}
}; 

module.exports = town;